package client.protocol;

import protocol.FailMarker;

/**
 * Decodes FailMarker received after request failed on server
 */
public class FailDecoder {
    /**
     * decodes FailMarker
     * @param marker marker received from server after failure
     * @return Message that describes failure
     */
    public static String decodeFailMarker(FailMarker marker) {
        switch (marker) {
            case NotLoggedIn:
                return "Nie zalogowany";
            case NotValidInput:
                return "Podano niewłaściwe dane";
            case SenderNotValid:
                return "Tożsamość użytkownika nie potwierdzona";
            case UserExists:
                return "Użytkownik o podanych danych już istnieje";
            case VerificationFailed:
                return "Niepoprawny login lub hasło";
            case AlreadyLoggedIn:
                return "Użytkownik już zalogowany";
            case UserNotExists:
                return "Użytkownik nie istnieje w systemie";
            case Unknown:
                return "Nieznany błąd";
        }
        return "Nieznany błąd";
    }
}
