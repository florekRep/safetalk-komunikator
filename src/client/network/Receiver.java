package client.network;

import client.security.ConnectionCrypter;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Handles receiving data from server
 * @author Florek
 */
public class Receiver extends Thread {
    private DataInputStream in; //reading stream
    private Dispatcher dispatcher; //dispatcher to send messages to
    private ConnectionCrypter crypter;  //crypter for connection

    /**
     * Initializes receiver
     * @param connection socket used for connection
     * @param crypter crypter used for connection
     */
    public Receiver(Socket connection, ConnectionCrypter crypter) throws IOException {
        in=new DataInputStream(connection.getInputStream());
        this.crypter=crypter;
    }

    /**
     * Sets dispatcher that will receive data
     * @param dispatcher dispatcher that will receive data from server
     */
    void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher=dispatcher;
    }

    /**
     * Runs in separate thread, waiting for server response
     */
    public void run() {
        try {
            while(!Thread.interrupted()) {
                int size=in.readInt();
                byte[] data=new byte[size];
                in.readFully(data);
                data=crypter.decrypt(data);
                if(dispatcher!=null)
                    dispatcher.received(data);
            }
        }  catch(IOException e) {
            //just ignore
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(dispatcher!=null)
                    dispatcher.disconnected();
                in.close();
            }
            catch(IOException ignore) { ignore.printStackTrace(); }
        }
    }
}
