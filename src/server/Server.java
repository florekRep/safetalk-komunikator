package server;

import server.database.DBConnection;
import server.logic.Reactor;
import server.requests.Dispatcher;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Entry Point for server application
 */
public class Server {
	public static final int PORT=3000; //default PORT to start server on
	public static void main(String[] args) throws Exception {
		Logger logger=Logger.getLogger("Server");   //configure logger (not really used that much...)
		FileHandler fh=new FileHandler("D:\\server.log");
		fh.setFormatter(new SimpleFormatter());
		logger.addHandler(fh);

        DBConnection.init();        //init database access
        Dispatcher.init(Executors.newFixedThreadPool(10));
		Thread server=new Thread(new Reactor(PORT));    //start reactor
        server.start();
        System.in.read(); //wait for enter to exit cleanly
        server.interrupt();
	}
}
