package client.responses;

import client.network.Dispatcher;
import client.protocol.ProtocolProcessor;
import protocol.FailMarker;
import protocol.Message;
import protocol.Protocol;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Handles events that come from server, by dispatching it. Uses same logic as AWT EventQueue
 * to receive events from server, listener must be provided
 * @author Florek
 */
public class ServerEventQueue extends Thread implements Dispatcher {
    private BlockingQueue<byte[]> receivedData=new LinkedBlockingQueue<>(5); //messages from server

    //registered listeners
    private MessageListener messageListener;
    private ResponseListener responseListener; //only one at the time can listen for both msg and response
    private KeyListener keyListener;
    private DisconnectionListener disconnectionListener;

    //locks
    private final Object lock=new Object();
    private boolean registered=false;

    /**
     * Sets message listener
     * @param listener message listener
     */
    public void setMessageListener(MessageListener listener) {
        messageListener=listener;
    }
    /**
     * Sets response listener
     * @param listener response listener
     */
    public void setResponseListener(ResponseListener listener) {
        responseListener=listener;
    }
    /**
     * Sets key listener
     * @param listener key listener
     */
    public void setKeyListener(KeyListener listener) {
        keyListener=listener;
    }
    /**
     * Sets disconnection listener
     * @param listener disconnection listener
     */
    public void setDisconnectedListener(DisconnectionListener listener) {
        disconnectionListener=listener;
    }

    /**
     * Invoked when user wants to wait for server response
     * usually invoked <u>after</u> sending message to server
     */
    public void waitForResponse() {
        synchronized (lock) {
            try {
                while(registered) {
                    lock.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /** notifies waiting thread about receiving response */
    private void notifyResponse() {
        synchronized (lock) {
            lock.notifyAll();
        }
    }
    /**
     * Invoked when user wants to register for wait for server response
     * usually invoked <u>before</u> sending message to server
     */
    public void registerForResponse() {
        registered=true;
    }

    /**
     * Invoked by connection to inform about disconnection
     */
    @Override
    public void disconnected() {
        if(disconnectionListener!=null)
            disconnectionListener.disconnected();
    }

    /**
     * Invoked by socket to add received data for dispatch
     * @param data data that is being added for processing
     */
    @Override
    public void received(byte[] data) {
        try {
            receivedData.put(data);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * EventQueue runs in separate thread to keep balance in work that threads do
     */
    @Override
    public void run() {
        try {
            while(!Thread.interrupted()) {
                byte[] data=receivedData.take();
                Protocol msg = ProtocolProcessor.getProtocol(data);
                switch(msg.request) {
                    case MESSAGE:
                        if(messageListener!=null)
                            messageListener.messageReceived((Message)msg.attachment);
                        break;

                    case MESSAGE_SENT:
                        if(messageListener!=null)
                            messageListener.messageSent((String)msg.attachment);
                        break;

                    case MESSAGE_FAILED:
                        if(messageListener!=null)
                            messageListener.messageFailed((String)msg.attachment);
                        break;

                    case SUCCESS:
                        if(responseListener!=null) {
                            responseListener.requestSuccessful(msg.attachment);
                            if(registered) {
                                registered=false;
                                notifyResponse();
                            }
                        }
                        break;

                    case FAIL:
                        if(responseListener!=null) {
                            responseListener.requestFailed((FailMarker)msg.attachment);
                            notifyResponse();
                        }
                        break;

                    case KEY:
                        if(keyListener!=null)
                            keyListener.keyReceived((byte[])msg.attachment);
                        break;

                    default: System.out.println("Unknown command: "+msg.request+" "+msg.attachment);
                }
            }
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }  catch(IOException e) {
            e.printStackTrace();
        }
    }
}
