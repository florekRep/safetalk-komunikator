package client;

import client.gui.MainWindow;
import client.security.SecurityOptions;

import javax.swing.*;
import java.awt.*;

/**
 * Entry point for client application
 */
public class Client {
    public static void main(String[] args) throws Exception {
        SecurityOptions.init();     //before running initialize SecurityOptions (for speed)
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); //change that awful Java look
        EventQueue.invokeLater(new Runnable() { //start the App
            public void run() {
                try {
                    new MainWindow();   //start application gui
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Uruchomienie nie powiodło się", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}
