package client.security;

import client.responses.KeyListener;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.PublicKey;

/**
 * Crypts connection between server and client
 * @author Florek
 */
public class ConnectionCrypter implements KeyListener {
    private static Cipher connectionCipher=SecurityOptions.getConnectionCipher();
    /** key used to crypt connection */
    private SecretKey connectionKey=null;

    /**
     * returns key used by server to encrypt connection key
     * @return public key to encrypt connection key
     */
    public PublicKey getHandshakeKey() {
        return SecurityOptions.getHandshakeKey().getPublic();
    }

    /**
     * Invoked when server establishes connection key
     * @param encodedKey key received from server
     */
    @Override
    public void keyReceived(byte[] encodedKey) {
        try {
            Cipher handshakeCipher=SecurityOptions.getHandshakeCipher();
            handshakeCipher.init(Cipher.DECRYPT_MODE,SecurityOptions.getHandshakeKey().getPrivate());
            byte[] decodedKey=handshakeCipher.doFinal(encodedKey);  //decrypt received key
            connectionKey=new SecretKeySpec(decodedKey,"AES");  //rebuild, and init connection key
        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if connection is secure
     * @return true if key was established and connection is crypted
     */
    public boolean isSecure() {
        return connectionKey!=null;
    }

    /**
     * encrypts data to send with established key
     * @param data data to be send
     * @return encrypted data
     */
    public synchronized byte[] encrypt(byte[] data) {
        if(!isSecure()) //if key was not established do nothing
            return data;
        try {
            connectionCipher.init(Cipher.ENCRYPT_MODE, connectionKey);
            return connectionCipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
            e.printStackTrace();
            return data;    //if encryption failed, return original data
        }
    }

    /**
     * decrypts data received from server
     * @param data data received
     * @return decrypted data
     */
    public synchronized byte[] decrypt(byte[] data) {
        if(!isSecure()) //if key was not established then do nothing
            return data;
        try {
            connectionCipher.init(Cipher.DECRYPT_MODE, connectionKey);
            return connectionCipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
            e.printStackTrace();    //if decryption failed, return original data
            return data;
        }
    }
}
