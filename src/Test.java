import client.security.SecurityOptions;
import protocol.Message;
import protocol.Protocol;
import protocol.Request;
import protocol.UserData;
import client.protocol.ProtocolProcessor;
import client.security.PasswordProcessor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Florek
 * Date: 19.01.13
 * Time: 08:59
 * To change this template use File | Settings | File Templates.
 */
public class Test {
    private static final Object o=new Object();
    public static void main(String... args) throws Exception {
        Socket s=new Socket("localhost",3000);
        DataInputStream in =new DataInputStream(s.getInputStream());
        DataOutputStream out=new DataOutputStream(s.getOutputStream());
        SecurityOptions.init();
        byte[] b= ProtocolProcessor.prepareToSend(
                new Protocol(
                        Request.LOGIN,new UserData("florek",
                        PasswordProcessor.hashPassword("1234".toCharArray()),null)));
        out.writeInt(b.length);
        out.write(b);

        b=ProtocolProcessor.prepareToSend(
          new Protocol(Request.MESSAGE, new Message("flors", "florek", "1".getBytes(), new Date().getTime()))
        );
        out.writeInt(b.length);
        out.write(b);

        b=ProtocolProcessor.prepareToSend(
                new Protocol(Request.MESSAGE, new Message("flors", "florek", "2".getBytes(), new Date().getTime()))
        );
        out.writeInt(b.length);
        out.write(b);
    }
}
