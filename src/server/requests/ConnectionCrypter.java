package server.requests;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

/**
 * Crypt connection between client and server
 */
public class ConnectionCrypter {
    private static KeyGenerator keygen; //static keygen for generating new keys for connection
    private static Cipher handshakeCipher; //also static Cipher object for handshake

    //Initializes used algorithms
    static {
        try {
            keygen=KeyGenerator.getInstance("AES");
            handshakeCipher=Cipher.getInstance("RSA");
            keygen.init(128); //AES 128
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    private Cipher connectionCipher;
    private SecretKey connectionKey;
    private boolean secure=false;

    public boolean isSecure() {
        return secure;
    }

    /**
     * Constructor generates new key for every connection
     */
    public ConnectionCrypter() throws NoSuchAlgorithmException, NoSuchPaddingException {
        connectionCipher=Cipher.getInstance("AES");
        synchronized (keygen) { //synchronize for safety
            connectionKey=keygen.generateKey();
        }
    }

    /**
     * Starts handshake by encrypting connection key with given public key
     * @param publicKey client public key
     * @return encrypted connection key
     */
    public byte[] performHandshaking(PublicKey publicKey)
            throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        synchronized (handshakeCipher) {    //synchronize on cipher
            handshakeCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            secure=true;
            return handshakeCipher.doFinal(connectionKey.getEncoded());
        }
    }

    /**
     * Decrypts received data (if possible)
     * @param data data from client
     * @return decrypted data
     */
    public synchronized byte[] decrypt(byte[] data) {
        if(!isSecure())
            return data;
        try {
            connectionCipher.init(Cipher.DECRYPT_MODE, connectionKey);
            return connectionCipher.doFinal(data);
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            return data;
        }
    }

    /**
     * Encrypts sending data (if possible)
     * @param data data to be send
     * @return encrypted data
     */
    public synchronized byte[] encrypt(byte[] data){
        if(!isSecure())
            return data;
        try {
            connectionCipher.init(Cipher.ENCRYPT_MODE,connectionKey);
            return connectionCipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
            return data;
        }
    }
}
