package client.gui;

/**
 * Application main frame (GUI)
 * @author Florek
 */

import client.logic.ApplicationLogic;
import client.logic.MainFrameNotify;
import protocol.ExUserData;
import protocol.UserData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class MainWindow implements MainFrameNotify {
    private JFrame mainFrame;
    private JList<UserData> contactsList;
    private DefaultListModel<UserData> contacts;
    private JLabel lblStatus;
    //dialogs
    private LoginDialog loginDialog=new LoginDialog();
    private AddUserDialog addUserDialog=new AddUserDialog();
    private AddContactDialog addContactDialog=new AddContactDialog();
    private UpdateAccountDialog updateAccountDialog=new UpdateAccountDialog();

    //Model
    private ApplicationLogic model=new ApplicationLogic(this);

    /**
     * Create the application.
     */
    public MainWindow() throws Exception{
        initGui();
        mainFrame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initGui() {
        mainFrame = new JFrame();
        mainFrame.setTitle("SafeTalk");
        mainFrame.setBounds(100, 100, 350, 500);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JMenuBar menuBar = new JMenuBar();
        mainFrame.setJMenuBar(menuBar);

        JMenu mnFile = new JMenu("SafeTalk");
        menuBar.add(mnFile);

        JMenuItem mntmConnect = new JMenuItem("Connect");
        mnFile.add(mntmConnect);

        JMenuItem mntmLogin = new JMenuItem("Login");
        mnFile.add(mntmLogin);

        JMenuItem mntmLogout = new JMenuItem("Logout");
        mnFile.add(mntmLogout);

        mnFile.addSeparator();

        JMenuItem mntmNewAccount = new JMenuItem("Nowe konto");
        mnFile.add(mntmNewAccount);

        JMenuItem mntmUpdateAccount = new JMenuItem("Zarzadzaj");
        mnFile.add(mntmUpdateAccount);

        JMenuItem mntmDeleteAccount = new JMenuItem("Usuń");
        mnFile.add(mntmDeleteAccount);

        mnFile.addSeparator();

        JMenuItem mntmDisconnect = new JMenuItem("Disconnect");
        mnFile.add(mntmDisconnect);

        JMenu mnContacts = new JMenu("Kontakty");
        menuBar.add(mnContacts);

        JMenuItem mntmAdd= new JMenuItem("Dodaj");
        mnContacts.add(mntmAdd);

        JMenuItem mntmDelete = new JMenuItem("Usuń");
        mnContacts.add(mntmDelete);

        JMenuItem mntmUpdate = new JMenuItem("Update");
        mnContacts.add(mntmUpdate);

        JMenuItem mntmArchive = new JMenuItem("Archiwum");
        mnContacts.add(mntmArchive);

        Font font=new Font("Helvetica",Font.PLAIN,14);

        contacts = new DefaultListModel<>();
        contactsList=new JList<>(contacts);
        contactsList.setFont(font);
        contactsList.setFixedCellHeight(30);
        JScrollPane scrollableContacts=new JScrollPane(contactsList);
        mainFrame.getContentPane().add(scrollableContacts, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        mainFrame.getContentPane().add(panel, BorderLayout.NORTH);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        lblStatus = new JLabel("Status: Ready to Connect");
        panel.add(lblStatus);

        mntmConnect.addActionListener(new ConnectEvent());
        mntmDisconnect.addActionListener(new DisconnectEvent());
        mntmLogin.addActionListener(new LoginEvent());
        mntmLogout.addActionListener(new LogoutEvent());
        mntmNewAccount.addActionListener(new NewAccountEvent());
        mntmUpdateAccount.addActionListener(new UpdateAccountEvent());
        mntmDeleteAccount.addActionListener(new DeleteAccountEvent());

        mntmArchive.addActionListener(new ArchiveEvent());
        mntmDelete.addActionListener(new DeleteContactEvent());
        mntmAdd.addActionListener(new AddContactEvent());
        mntmUpdate.addActionListener(new UpdateContactEvent());

        mainFrame.addWindowListener(new WindowClosingEvent());
        contactsList.addMouseListener(new StartConversationEvent());
    }

    /*            HELP FUNCTIONS           */

    /** lists all contacts on frame */
    private void listContacts() {
        contacts.clear();
        for(UserData user: model.getContacts())
            contacts.addElement(user);
        contactsList.revalidate();
        contactsList.repaint();
    }

    /*              EVENT HANDLERS           */

    private class ConnectEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.connect();
        }
    }
    private class DisconnectEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.disconnect();
        }
    }
    private class LoginEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(loginDialog.showLoginDialog(mainFrame)==JOptionPane.OK_OPTION) {
                UserData ud=new UserData(loginDialog.login(), loginDialog.password(), null);
                if(ud.login==null || ud.pass==null)
                    failed("Puste pola!");
                else
                    model.login(ud);
            }
        }
    }
    private class LogoutEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.logout();
        }
    }

    private class NewAccountEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(addUserDialog.showAddUserDialog(mainFrame)==JOptionPane.OK_OPTION) {
                ExUserData userData=new ExUserData(addUserDialog.login(),addUserDialog.password(),
                        addUserDialog.name(),null,null);
                if(userData.login==null || userData.pass==null)
                    failed("Puste pola!");
                else
                    model.createAccount(userData);
            }
        }
    }
    private class UpdateAccountEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(updateAccountDialog.showUpdateAccountDialog(mainFrame)==JOptionPane.OK_OPTION) {
                ExUserData user=new ExUserData(null,updateAccountDialog.password(),null,null,null);
                if(updateAccountDialog.newName()!=null)
                    user.name=updateAccountDialog.newName();
                if(updateAccountDialog.newPassword()!=null)
                    user.newPass=updateAccountDialog.newPassword();
                if(user.pass!=null)
                    model.updateAccount(user,updateAccountDialog.generateNewKey());
                else
                    failed("Puste pola!");
            }
        }
    }
    private class DeleteAccountEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(loginDialog.showLoginDialog(mainFrame)==JOptionPane.OK_OPTION) {
                UserData ud=new UserData(loginDialog.login(), loginDialog.password(),null);
                if(ud.login==null || ud.pass==null)
                    failed("Puste pola!");
                else
                    model.deleteAccount(ud);
            }
        }
    }

    private class ArchiveEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.getArchiveViewer().showWindow();
        }
    }
    private class AddContactEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(addContactDialog.showAddContactDialog(mainFrame)==JOptionPane.OK_OPTION) {
                ExUserData newContact=new ExUserData(addContactDialog.login(),null,
                        addContactDialog.name(),null,null);
                if(newContact.login==null)
                    failed("Puste pola!");
                else
                    model.addContact(newContact);
            }
        }
    }
    private class DeleteContactEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.deleteContact(contactsList.getSelectedValue());
            listContacts();
        }
    }
    private class UpdateContactEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            UserData ud=contactsList.getSelectedValue();
            model.updateContact(new ExUserData(ud.login,null,ud.name,null));
        }
    }

    private class WindowClosingEvent extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent e) {
            model.disconnect();
            super.windowClosing(e);
        }
    }
    private class StartConversationEvent extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent evt) {
            JList<UserData> list = (JList<UserData>)evt.getSource();
            if(evt.getClickCount()==2)
                model.startConversation(list.getSelectedValue());
        }
    }

    //notifications
    @Override
    public void failed(String msg) {
        JOptionPane.showMessageDialog(mainFrame,msg,"Błąd",JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void info(String msg) {
        JOptionPane.showMessageDialog(mainFrame,msg,"Błąd",JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void setStatus(String status) {
        lblStatus.setText("Status: "+status);
    }

    @Override
    public void refresh() {
        listContacts();
    }
}