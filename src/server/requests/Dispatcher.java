package server.requests;

import protocol.*;
import server.exceptions.*;
import server.logic.ConnectionHandler;
import server.messages.MessageManager;
import server.protocol.ProtocolProcessor;
import server.users.ConnectionManager;
import server.users.UserDB;

import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Dispatches messages from given connectionHandler (channel)
 */
public class Dispatcher implements Runnable {
    private static Logger logger= Logger.getLogger("Server");

    private static Executor threadPool;
    public static void init(Executor executor) {
        threadPool=executor;
    }

    //managers and database handlers
    private static ConnectionManager connections=ConnectionManager.getConnectionManager();
    private static MessageManager messageManager=MessageManager.getMessageManager();
    private static UserDB userDB=UserDB.getUserDB();

    //queue of messages to dispatch
    private final Queue<byte[]> messages = new ConcurrentLinkedQueue<>();
    boolean queued=false; //for sync
    private final Object lock=new Object(); //for sync, to dispatch messages in order of arrival

    //handler and user login
    private ConnectionHandler handler;
    private String login;

    //crypter used to encrypt connection
    private ConnectionCrypter crypter;

    /**
     * Return crypter used in thi
     * @return
     */
    public ConnectionCrypter getCrypter() {
        return crypter;
    }

    public Dispatcher(ConnectionHandler handler) {
        this.handler=handler;
        try {
            crypter=new ConnectionCrypter();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public void disconnected() {
        if(login!=null) {
            connections.handleDisconnection(login);
            messages.clear();
        }
    }

    public void addRequest(byte[] request) {
        messages.add(request);
        synchronized (lock) {
            if(!queued) {   //if not already queued in executor
                threadPool.execute(this); //add it
                queued=true;
            }
        }
    }


    public void sendWaitingMessages(String login) throws SQLException, IOException{
        LinkedList<Message> msgs=messageManager.getWaitingMessages(login);
        ConnectionHandler connectionHandler=connections.getConnection(login);
        for(Message msg: msgs)
            sendTo(connectionHandler,new Protocol(Request.MESSAGE, msg));

    }

    public void run() {
        try {
            byte[] data=messages.poll();
            Protocol current = ProtocolProcessor.getProtocol(crypter.decrypt(data));
            switch (current.request) {
                case ADD_USER:
                    userDB.addUser((ExUserData)current.attachment);
                    returnToSender(new Protocol(Request.SUCCESS,null));
                    break;

                case LOGIN:
                    if(login!=null)
                        throw new AlreadyLoggedIn();
                    connections.loginUser((UserData)current.attachment,handler);
                    login=((UserData)current.attachment).login;
                    returnToSender(new Protocol(Request.SUCCESS,null));
                    if(messageManager.hasWaitingMessages(((UserData)current.attachment).login))
                        sendWaitingMessages(((UserData)current.attachment).login);
                    break;

                case LOGOUT:
                    connections.logoutUser((UserData)current.attachment);
                    login=null;
                    returnToSender(new Protocol(Request.SUCCESS,null));
                    break;

                case GET_USER:
                    userDB.getUserData((ExUserData)current.attachment);
                    returnToSender(new Protocol(Request.SUCCESS,current.attachment));
                    break;

                case REMOVE_USER:
                    userDB.delete((UserData)current.attachment);
                    returnToSender(new Protocol(Request.SUCCESS,null));
                    break;

                case UPDATE_USER:
                    userDB.update((ExUserData)current.attachment);
                    returnToSender(new Protocol(Request.SUCCESS,null));
                    break;

                case KEY:
                    byte[] toSend=crypter.performHandshaking((PublicKey)current.attachment);
                    handler.send(ProtocolProcessor.prepareToSend(new Protocol(Request.KEY,toSend))); //no encryption in this one!
                    break;

                case MESSAGE:       //messages are treated as separate channel of information, so it doesn't use standard protocol response Enums
                    Message received=(Message)current.attachment;
                    try {           //that's why extra try block
                        if(!connections.isLoggedIn(received.from) || !connections.getConnection(received.from).equals(handler))
                            throw new SenderNotValid();
                        if(connections.isLoggedIn(received.to)) {   //if logged in
                            if(messageManager.hasWaitingMessages(received.to))  //just in case a message was registered when user was logging in
                                sendWaitingMessages(received.to);
                            ConnectionHandler connectionHandler=connections.getConnection(received.to);
                            sendTo(connectionHandler,current);
                        } else {
                            messageManager.storeMessage(received);  //else store in db to later send it
                        }
                        returnToSender(new Protocol(Request.MESSAGE_SENT,received.to));
                    } catch (Exception e) {
                        reportMessageFailed(e,received.to);
                    }
                    break;

                default:
                    reportException(new UnsupportedOperationException());
            }
        } catch (Exception e) {  //return information to Client about fail
            e.printStackTrace();
             reportException(e);
        }
        //at the end, check if any requests are pending
        synchronized (lock) {
            if(!messages.isEmpty())         //if any more messages than add task to executor again
                 threadPool.execute(this);
            else
                queued=false;               //if not then allow for queue
        }
    }

    private void returnToSender(Protocol protocol) throws IOException {
        this.handler.send(crypter.encrypt(
                ProtocolProcessor.prepareToSend(protocol)
        ));
    }

    private void sendTo(ConnectionHandler handler, Protocol protocol) throws IOException{
        ConnectionCrypter crypter=handler.getCrypter();
        handler.send(crypter.encrypt(
                ProtocolProcessor.prepareToSend(protocol)
        ));
    }

    private FailMarker getMarker(Exception e) {
        FailMarker marker;
        if(e instanceof NotValidInput)
            marker=FailMarker.NotValidInput;
        else if(e instanceof UserExists)
            marker=FailMarker.UserExists;
        else if(e instanceof  SenderNotValid)
            marker=FailMarker.SenderNotValid;
        else if(e instanceof NotLoggedIn)
            marker=FailMarker.NotLoggedIn;
        else if(e instanceof VerificationFailed)
            marker=FailMarker.VerificationFailed;
        else if(e instanceof AlreadyLoggedIn)
            marker=FailMarker.AlreadyLoggedIn;
        else if(e instanceof UserNotExists)
            marker=FailMarker.UserNotExists;
        else
            marker= FailMarker.Unknown;
        return marker;
    }

    private void reportException(Exception e) {
        try {
            returnToSender(new Protocol(Request.FAIL,getMarker(e)));
        } catch(IOException ignore) {
            ignore.printStackTrace();
            logger.severe(e.getMessage());
        }
    }

    private void reportMessageFailed(Exception e, String to) {
        try {
            returnToSender(new Protocol(Request.MESSAGE_FAILED,to));
        } catch(IOException ignore) {
            ignore.printStackTrace();
            logger.severe(e.getMessage());
        }
    }
}
