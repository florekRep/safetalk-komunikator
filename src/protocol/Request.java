package protocol;

/**
 * Request that server or client understands
 */

import java.io.Serializable;

public enum Request implements Serializable{
    ADD_USER, REMOVE_USER, GET_USER, UPDATE_USER, LOGIN, LOGOUT, SUCCESS, MESSAGE, MESSAGE_SENT, MESSAGE_FAILED, FAIL, KEY
}
