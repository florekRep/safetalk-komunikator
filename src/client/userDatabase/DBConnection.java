package client.userDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Manages connection to the DB, also initializes ArchiveManager and ContactManager
 * @author Florek
 */
public class DBConnection {
    private static final String DRIVER="org.sqlite.JDBC";
    private static final String DBNAME="jdbc:sqlite:";
    private Connection dbConnection=null;

    private ArchiveManager archive;
    private ContactManager contacts;

    /**
     * Creates new DBConnection, for logged user. Also creates related archive and contacts managers
     * @param login user that we want to get database connection for
     */
    public DBConnection(String login) throws SQLException {
        try {
            Class.forName(DRIVER);
            dbConnection=DriverManager.getConnection(DBNAME+login+".db");
            archive=new ArchiveManager(dbConnection);
            contacts=new ContactManager(dbConnection);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes opened database
     */
    public void close() {
        try {
            dbConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets ArchiveManager associated with connection (unique for every user)
     * @return archive manager
     */
    public ArchiveManager getArchiveDB() {
        return archive;
    }
    /**
     * Gets ContactManager associated with connection (unique for every user)
     * @return contact manager
     */
    public ContactManager getContactsDB() {
        return contacts;
    }
}
