package client.security;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

/**
 * Manages all ciphers and keygens used in client. Singleton
 * @author Florek
 */
public class SecurityOptions {
    private static KeyPair handshakeKey;
    private static KeyPairGenerator RSAKeygen;
    private static KeyGenerator DESKeygen;

    private static Cipher RSACipher;
    private static Cipher AESCipher;
    private static Cipher DESCipher;
    private static MessageDigest hash;
    private static Signature signature;

    /**
     * Initializes all crypt algorithms used in program
     */
    public static void init() throws NoSuchAlgorithmException, NoSuchPaddingException {
        //init keygens
        RSAKeygen=KeyPairGenerator.getInstance("RSA");
        RSAKeygen.initialize(512);
        DESKeygen=KeyGenerator.getInstance("DES");
        DESKeygen.init(56);
        //init ciphers
        RSACipher=Cipher.getInstance("RSA");
        AESCipher=Cipher.getInstance("AES");
        DESCipher=Cipher.getInstance("DES");
        signature=Signature.getInstance("SHA1withRSA");
        hash=MessageDigest.getInstance("SHA1");

        //also for speed purpouse generate KeyPair for handshake
        handshakeKey=RSAKeygen.generateKeyPair();
    }

    //self explaining
    public static KeyPair getHandshakeKey() {
        return handshakeKey;
    }

    public static MessageDigest getHash() {
        return hash;
    }

    public static Cipher getHandshakeCipher() {
        return RSACipher;
    }

    public static Cipher getConnectionCipher() {
        return AESCipher;
    }

    public static KeyPairGenerator getUserKeyGenerator() {
        return RSAKeygen;
    }

    public static Cipher getUserKeyCipher() {
        return RSACipher;
    }

    public static Cipher getMessageCipher() {
        return DESCipher;
    }

    public static KeyGenerator getMessageKeygen() {
        return DESKeygen;
    }

    public static Signature getSignature() {
        return signature;
    }
}
