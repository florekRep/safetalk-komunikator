package client.messages;

import client.gui.MessageWindow;
import client.network.Connection;
import client.responses.MessageListener;
import client.userDatabase.ArchiveManager;
import client.userDatabase.ContactManager;
import protocol.Message;
import protocol.Protocol;
import protocol.Request;
import protocol.SafeMessage;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller for message windows
 * @author Florek
 */
public class MessagesDispatcher implements MessageListener{
    private static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); //data formatter

    private Map<String, MessageWindow> activateWindows=new HashMap<>(); //map of opened window
    private String user;                //logged user
    private ArchiveManager archive;     //archive manager
    private ConversationCrypter conversationCrypter;    //crypter for messages
    private Connection connection;      //reference to connection

    /**
     * Creates new controller for windows, unique for every user
     * @param connection connection
     * @param user logged user
     * @param archive archive manager
     * @param contacts contacts manager
     */
    public MessagesDispatcher(Connection connection, String user, ArchiveManager archive, ContactManager contacts) {
        this.connection=connection;
        this.user=user;
        this.archive=archive;
        conversationCrypter=new ConversationCrypter(contacts);
    }

    /**
     * Sends message, invoked from message window
     * @param wnd window that invokes method
     * @param to user to whom it is addressed
     * @param message message itself
     * @param time time of sending
     * @param safe send using safe channel
     */
    public void send(MessageWindow wnd, String to, String message, Date time, boolean safe) {
        Message msg;
        wnd.writeMessage(sdf.format(time)+" "+user+": "+message); //display message. And create appropriate structure
        if(safe)
            msg=new SafeMessage(to,user,message.getBytes(),time.getTime(),null,null);
        else
            msg=new Message(to,user,message.getBytes(),time.getTime());

        archive.addMessage(msg);    //archive message

        if(safe) {  //encrypt if selected and if possible (ie all keys available)
            if(conversationCrypter.encode((SafeMessage)msg))
                wnd.setInfo("Sending message on safe channel");
            else {
                msg=new Message(msg.to, msg.from, msg.msg, msg.time);
                wnd.setInfo("Unable to send on safe channel");
            }
        } else {
            wnd.setInfo("Sending normal message");
        }
        connection.send(new Protocol(Request.MESSAGE,msg)); //send to server
        wnd.setStatus("Sending to server");
    }

    /**
     * Performs necessary closing operations
     * @param to window identifier
     */
    public void closing(String to) {
        activateWindows.remove(to);
        conversationCrypter.close(to); //also close related crypter
    }

    /** creates new window */
    private MessageWindow createNewWindow(String to) {
        final MessageWindow window=new MessageWindow(this, to);
        activateWindows.put(to,window);
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                window.setVisible(true);
            }
        });
        return window;
    }

    /**
     * Starts conversation
     * @param with User with whom to start conversation
     */
    public void startConversation(String with) {
        if(!activateWindows.containsKey(with))
            createNewWindow(with);
    }

    /**
     * Closes all windows
     */
    public void closeAll() {
        for(Map.Entry<String,MessageWindow> entry: activateWindows.entrySet())
            entry.getValue().dispose();
        activateWindows.clear();
        conversationCrypter.closeAll();
    }

    /**
     * Invoked after receiving message from server
     * @param msg received message
     */
    @Override
    public void messageReceived(Message msg) {
        MessageWindow window=activateWindows.get(msg.from);
        if(window==null)
            window=createNewWindow(msg.from);
        if(msg instanceof SafeMessage) {
            SafeMessageModule.Validation val= conversationCrypter.decode((SafeMessage)msg);
            if(val== SafeMessageModule.Validation.KEY_OUT_OF_DATE)
                msg.msg="Unable to decrypt. Key may be out of date".getBytes();
            window.setInfo("Message received on safe channel, identity: "+val);
        } else {
            window.setInfo("Message received");
        }
        archive.addMessage(msg);
        window.writeMessage(sdf.format(new Date(msg.time))+" "+msg.from+": "+new String(msg.msg));
        window.setStatus("Received");
    }

    /**
     * Invoked when message was successfully sent
     * @param to message to whom message was sent
     */
    @Override
    public void messageSent(String to) {
        MessageWindow window=activateWindows.get(to);
        window.setStatus("Sent");
    }

    /**
     * Invoked when message failed to be delivered
     * @param to user to whom message was supposed to be send
     */
    @Override
    public void messageFailed(String to) {
        MessageWindow window=activateWindows.get(to);
        window.setStatus("Failed");
    }
}