package server.users;

import protocol.ExUserData;
import protocol.UserData;
import server.exceptions.NotValidInput;
import server.exceptions.UserExists;
import server.exceptions.UserNotExists;
import server.exceptions.VerificationFailed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Manages user database on server side
 * @author Florek
 */
public class UserDB {
    private static UserDB dataBase;
    private static Logger logger=Logger.getLogger("Server");

    private Connection conn;    //connection to db
    /** Constructor initializes connection */
    private UserDB(Connection conn) {
        this.conn =conn;
    }

    /**
     * Return instance of manager (Singleton)
     * @return instance of UserDB
     */
    public static UserDB getUserDB() {
        if(dataBase==null) {
            logger.severe("UserDB not initialized");
            throw new RuntimeException("UserDB not initialized");
        }
        return dataBase;
    }

    /**
     * Initializes manager (create table)
     * @param conn connection to database
     */
    public static void init(Connection conn) throws SQLException {
        dataBase=new UserDB(conn);
        try (PreparedStatement stmt=conn.prepareStatement(
            "CREATE TABLE IF NOT EXISTS users (" +
            "id INTEGER NOT NULL PRIMARY KEY, " +
            "login VARCHAR(64) NOT NULL, " +
            "pass BLOB, " +
            "userName VARCHAR(64), " +
            "publicKey BLOB NOT NULL, " +
            "salt BLOB NOT NULL)"
        )) {   //autoCloseable
            stmt.executeUpdate();
        } catch(SQLException e) {
            close();
            throw e;
        }
    }

    /**
     * Close manager
     */
    public static void close() {
        logger.info("Closing database");
        dataBase=null;
    }

    /**
     * Add user to database
     * @param user user to be added
     */
    public void addUser(ExUserData user) throws SQLException, NotValidInput, UserExists {
        if(user.pass==null || user.login==null || user.publicKey==null)
            throw new NotValidInput();
        if(exists(user))
            throw new UserExists();
        try(PreparedStatement stmt=conn.prepareStatement(
                "INSERT INTO users (login, pass, userName, publicKey, salt) VALUES(?,?,?,?,?)"
        )) {
            PasswordProcessor pass= PasswordProcessor.getInstance();
            byte[] salt=pass.getRandomSalt();
            stmt.setString(1, user.login);
            stmt.setBytes(2, pass.hash(user.pass, salt));
            stmt.setString(3, user.name);
            stmt.setBytes(4, user.publicKey);
            stmt.setBytes(5,salt);
            stmt.executeUpdate();
        }
    }
    /** Check if given user exists in database */
    private boolean exists(UserData user) throws SQLException {
        try(PreparedStatement stmt=conn.prepareStatement("SELECT 1 FROM users WHERE login=?")) {
            stmt.setString(1,user.login);
            return stmt.executeQuery().next();
        }
    }

    /**
     * Verify if given user data (login, pass) matches those in database
     * @param user user to be verified
     * @return true if matches, false otherwise
     */
    public boolean verifyUser(UserData user) throws SQLException, UserNotExists {
        try(PreparedStatement stmt=conn.prepareStatement(
                "SELECT * FROM users WHERE login=?"
        )) {
            PasswordProcessor passProcessor=PasswordProcessor.getInstance();
            stmt.setString(1,user.login);
            ResultSet rs=stmt.executeQuery();
            if(rs.next()) {
                byte[] salt=rs.getBytes(6);
                byte[] hash=rs.getBytes(3);
                return passProcessor.verify(hash,salt,user.pass);
            }
            throw new UserNotExists();
        }
    }

    /**
     * Update user data with key and name
     * @param user user to be updated
     */
    public void getUserData(ExUserData user) throws SQLException, UserNotExists {
        try(PreparedStatement stmt=conn.prepareStatement(
            "SELECT * FROM users WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            ResultSet rs=stmt.executeQuery();
            if(rs.next()) {
                user.publicKey=rs.getBytes(5);
                user.name=rs.getString(4);
            } else
                throw new UserNotExists();
        }
    }

    /** Completes user data with missing fields */
    private void complete(ExUserData user) throws SQLException, UserNotExists {
        try(PreparedStatement stmt=conn.prepareStatement(
            "SELECT * FROM users WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            ResultSet rs=stmt.executeQuery();
            if(rs.next()) {
                if(user.publicKey==null) user.publicKey=rs.getBytes(5);
                if(user.name==null) user.name=rs.getString(4);
            } else
                throw new UserNotExists();
        }
    }

    /**
     * Update user data
     * @param user new user data
     */
    public void update(ExUserData user) throws SQLException, VerificationFailed, UserNotExists {
        if(!verifyUser(user))
            throw new VerificationFailed();
        complete(user);
        if(user.newPass!=null) user.pass=user.newPass;
        byte[] salt;
        try(PreparedStatement stmt=conn.prepareStatement(
                "SELECT * FROM users WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            ResultSet rs=stmt.executeQuery();
            salt=rs.getBytes(6);
        }
        try(PreparedStatement stmt=conn.prepareStatement(
            "UPDATE users SET pass=?, userName=?, publicKey=? WHERE login=?"
        )) {
            PasswordProcessor passwordProcessor=PasswordProcessor.getInstance();
            stmt.setBytes(1,passwordProcessor.hash(user.pass,salt));
            stmt.setString(2,user.name);
            stmt.setBytes(3,user.publicKey);
            stmt.setString(4,user.login);
            stmt.executeUpdate();
        }

    }

    /**
     * Remove user from database
     * @param user user to be removed
     */
    public void delete(UserData user) throws SQLException, VerificationFailed, UserNotExists {
        if(!verifyUser(user))
            throw new VerificationFailed();
        try(PreparedStatement stmt=conn.prepareStatement(
            "DELETE FROM users WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            stmt.executeUpdate();
        }
    }
}
