package server.logic;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Reactor class. Responsible for maintaining server, accepting connections, and demultiplexing multiple clients
 * and what is important for performing basic IO operations (that includes reading/writing)
 * Save trouble with synchronizing Threads to do that (Threads responsible for carrying out requests)
 * It is NOT responsible for handling protocol requests. 
 * 
 * @author Florek
 */
public class Reactor implements Runnable {
	private int port;
	private volatile boolean stopped=false;  //used to stop Reactor
	private ServerSocketChannel serverChannel;
	private Selector selector;
	private Logger logger=Logger.getLogger("Server");
	

	public Reactor(int port) {
		this.port=port;
	}
	private void initialize() throws IOException {
		//opening channel for server
		serverChannel=ServerSocketChannel.open();
		serverChannel.configureBlocking(false);	//async
		InetAddress hostAddress=InetAddress.getByName(null); //localhost
		serverChannel.bind(new InetSocketAddress(hostAddress,port)); //binding on selected port
			
		logger.info("Server established on: "+serverChannel.getLocalAddress());
		//Creating selector
		selector=Selector.open();
		//register handler for acceptance
		serverChannel.register(selector, SelectionKey.OP_ACCEPT,
				new Acceptor(selector,serverChannel));
	}
	public void run() {
		try {
			initialize();
			while(!stopped) {	//while running
				selector.select(30000);	//select keys with events with timeout
				Iterator<SelectionKey> it=selector.selectedKeys().iterator();
				while(it.hasNext()) {
					SelectionKey selKey=it.next();	//get key
					it.remove();	//remove from Set
					if(selKey.isValid() && selKey.isAcceptable()) {	//on Acceptable
						Acceptor acceptor=(Acceptor)selKey.attachment();
						acceptor.accept();
					}
                    if(selKey.isValid() && selKey.isWritable())    //on writable
                        write(selKey);
					if(selKey.isValid() && selKey.isReadable()) 	//on readable
						read(selKey);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.warning(e.getMessage());
		}  
	}
	
	void write(SelectionKey key) throws IOException {
		//get channel to write to
		SocketChannel socketChannel=(SocketChannel)key.channel();
		//get connection handler
		ConnectionHandler handler=(ConnectionHandler)key.attachment();
		//get responses Queue for given Channel
		Queue<byte[]> queue=handler.getMessages();
        ByteBuffer buffer=handler.getToWrite();
        synchronized (buffer) {
            //write all responses
            while(!queue.isEmpty() || buffer.hasRemaining()) {
                while(buffer.hasRemaining())
                    if(socketChannel.write(buffer)==0)
                        return;     //write buffer full wait for next opportunity (do not register for read)
                buffer.clear();
                byte[] data=queue.poll();
                buffer.putInt(data.length);
                buffer.put(data);
                buffer.flip();
                while(buffer.hasRemaining())
                    if(socketChannel.write(buffer)==0) 	//actual write operation
                        return;     //same as above
            }
            //if done writing, change interest to reading socket
            //OP_WRITE ONLY when there is something to write
            if(queue.isEmpty() && !buffer.hasRemaining())
                key.interestOps(SelectionKey.OP_READ);   //change to Reading again
        }
	}
	
	private void read(SelectionKey key) throws IOException {
		//get channel to read from
		SocketChannel socketChannel=(SocketChannel)key.channel(); 
		ConnectionHandler handler=(ConnectionHandler)key.attachment();
		ByteBuffer buffer=handler.getChannelBuffer();
		int numRead;
		try {
			numRead=socketChannel.read(buffer);
		} catch(IOException e) {
			//Channel has been closed forcibly, cancel selection key and close channel
			logger.severe("Client: "+socketChannel.getRemoteAddress()+" :connection Lost");
            handler.connectionClosed();
			socketChannel.close();
			return;
		}
		if(numRead==-1) {
			//Channel closed cleanly
            handler.connectionClosed();
			logger.info("Client: "+socketChannel.getRemoteAddress()+" disconnected");
			socketChannel.close();
            return;
		}
		buffer.flip();
		//handing read bytes to Channel handler
		handler.readMessage(numRead);
	}
}
