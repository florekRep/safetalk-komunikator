package client.responses;

/**
 * Interface that is implemented by classes that want to react to receiving key from server
 * @author Florek
 */
public interface KeyListener {
    public void keyReceived(byte[] encodedKey);
}
