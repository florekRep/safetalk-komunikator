package client.logic;

/**
 * Implemented by class that will represent all kinds of messages (MainFrame)
 */
public interface MainFrameNotify {
    public void failed(String msg);
    public void info(String msg);
    public void setStatus(String status);
    public void refresh();
}
