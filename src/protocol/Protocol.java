package protocol;

/**
 * Object used in communication
 */

import java.io.Serializable;

public class Protocol implements Serializable {
	public Request request;
	public Object attachment;
	public Protocol(Request request, Object attach) {
		this.request=request;
		this.attachment=attach;
	}
}
