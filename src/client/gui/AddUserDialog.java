package client.gui;

import client.security.PasswordProcessor;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Arrays;

public class AddUserDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JTextField loginField;
    private JPasswordField passwordField;
    private JTextField nameField;

    /**
     * Return entered password
     * @return hashed password
     */
    public byte[] password() {
        if(passwordField.getPassword().length==0)
            return null;
        byte[] pass = PasswordProcessor.hashPassword(passwordField.getPassword());
        Arrays.fill(passwordField.getPassword(),'\0'); //security reasons
        return pass;
    }

    /**
     * Return entered login
     * @return login
     */
    public String login() {
        return loginField.getText().equals("") ? null : loginField.getText();
    }

    /**
     * Return entered name
     * @return name
     */
    public String name() {
        return nameField.getText().equals("") ? null : nameField.getText();
    }

    /**
     * Show dialog and waits for input
     * @param parent parent window
     * @return user action
     */
    public int showAddUserDialog(Component parent) {
        loginField.setText("");
        passwordField.setText("");
        nameField.setText("");
        return JOptionPane.showConfirmDialog(parent,contentPanel,"Add user",JOptionPane.OK_CANCEL_OPTION);
    }

    /**
     * Create the dialog.
     */
    public AddUserDialog() {
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new MigLayout("", "[][grow]", "[][][][]"));
        {
            JLabel lblLogin = new JLabel("Login");
            contentPanel.add(lblLogin, "cell 0 0,alignx trailing");
        }
        {
            loginField = new JTextField();
            contentPanel.add(loginField, "cell 1 0,growx");
            loginField.setColumns(20);
        }
        {
            JLabel lblHaslo = new JLabel("Has\u0142o");
            contentPanel.add(lblHaslo, "cell 0 1,alignx trailing");
        }
        {
            passwordField = new JPasswordField();
            passwordField.setColumns(20);
            contentPanel.add(passwordField, "cell 1 1,growx");
        }
        {
            JLabel lblNazwaop = new JLabel("Nazwa(op)");
            contentPanel.add(lblNazwaop, "cell 0 3,alignx trailing");
        }
        {
            nameField = new JTextField();
            contentPanel.add(nameField, "cell 1 3,growx");
            nameField.setColumns(20);
        }
        pack();
    }
}