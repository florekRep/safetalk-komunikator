package client.gui;

import client.security.PasswordProcessor;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Arrays;

public class UpdateAccountDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JPasswordField password;
    private JTextField name;
    private JPasswordField newPassword;
    private JCheckBox newKey;

    /**
     * Show dialog and waits for input
     * @param parent parent window
     * @return user action
     */
    public int showUpdateAccountDialog(Component parent) {
        name.setText("");
        password.setText("");
        newPassword.setText("");
        newKey.setSelected(false);
        return JOptionPane.showConfirmDialog(parent, contentPanel, "Update", JOptionPane.OK_CANCEL_OPTION);
    }

    /**
     * Return entered name
     * @return name
     */
    public String newName() {
        return name.getText().equals("") ? null : name.getText();
    }

    /**
     * Return entered password
     * @return hashed password
     */
    public byte[] password() {
        if(password.getPassword().length==0)
            return null;
        byte[] pass = PasswordProcessor.hashPassword(password.getPassword());
        Arrays.fill(password.getPassword(),'\0'); //security reasons
        return pass;
    }

    /**
     * Return entered new password
     * @return hashed new password
     */
    public byte[] newPassword() {
        if(newPassword.getPassword().length==0)
            return null;
        byte[] pass = PasswordProcessor.hashPassword(newPassword.getPassword());
        Arrays.fill(newPassword.getPassword(),'\0'); //security reasons
        return pass;
    }

    /**
     * Return if generate new key was selected
     * @return whether to generate new key for user
     */
    public boolean generateNewKey() {
        return newKey.isSelected();
    }


    /**
     * Create the dialog.
     */
    public UpdateAccountDialog() {
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new MigLayout("", "[][grow]", "[][][][][]"));
        {
            JLabel lblName = new JLabel("Nowa Nazwa");
            contentPanel.add(lblName, "cell 0 0,alignx trailing");
        }
        {
            name = new JTextField();
            contentPanel.add(name, "cell 1 0,growx");
            name.setColumns(20);
        }
        {
            JLabel lblPass = new JLabel("Has\u0142o");
            contentPanel.add(lblPass, "cell 0 1,alignx trailing");
        }
        {
            password = new JPasswordField();
            password.setColumns(20);
            contentPanel.add(password, "cell 1 1,growx");
        }
        {
            JLabel lblNewPass = new JLabel("Nowe Has\u0142o");
            contentPanel.add(lblNewPass, "cell 0 2,alignx trailing");
        }
        {
            newPassword = new JPasswordField();
            newPassword.setColumns(20);
            contentPanel.add(newPassword, "cell 1 2,growx");
        }
        {
            newKey = new JCheckBox("nowy klucz");
            contentPanel.add(newKey, "cell 1 3");
        }
        {
            JLabel lblWarning = new JLabel("<html>Generacja nowego klucza nie jest zalecana,<br>\r\nje\u017Celi nie istnieje podej\u017Cenie \u017Ce stary klucz zosta\u0142 skradziony.<br>\r\nSpowoduje to konieczno\u015B\u0107 recznej aktualizacji <br>\r\ntwojego konta przez znajomych</html>");
            contentPanel.add(lblWarning, "cell 1 4");
        }
        pack();
    }

}
