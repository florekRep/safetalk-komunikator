package client.userDatabase;

import protocol.ExUserData;
import protocol.UserData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Manages contacts for user
 * @author Florek
 */
public class ContactManager {
    /** connection to database */
    private Connection dbConnection;

    /**
     * Initializes Contact Manager with given connection (unique for every user)
     * @param conn connection to database
     */
    ContactManager(Connection conn) throws SQLException {
        dbConnection=conn;
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "CREATE TABLE IF NOT EXISTS contacts (" +
            "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "login VARCHAR(64) NOT NULL ," +
            "userKey BLOB NOT NULL, " +
            "userName VARCHAR(64))"
        )) {
            stmt.executeUpdate();
        }
    }

    /**
     * Retrieves all contacts for logged user
     * @return list of contacts for logged user
     */
    public List<UserData> getContacts() throws SQLException {
        List<UserData> list=new LinkedList<>();
        try(PreparedStatement stmt= dbConnection.prepareStatement(
           "SELECT * FROM contacts"
        )) {
            ResultSet rs=stmt.executeQuery();
            while(rs.next()) {
                UserData user=new UserData(rs.getString(2),null,rs.getString(4));
                list.add(user);
            }
        }
        return list;
    }

    /**
     * Retrieves public key for given user
     * @param user user to retrieve key for
     * @return  user's public key
     */
    public byte[] getUserKey(UserData user) throws SQLException {
        try(PreparedStatement stmt=dbConnection.prepareStatement(
            "SELECT * FROM contacts WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            ResultSet rs=stmt.executeQuery();
            if(rs.next())
                return rs.getBytes(3);
            return null;
        }
    }

    /**
     * Checks if user exists
     * @param user user to check existence for
     * @return true if user exists in contacts, false otherwise
     */
    private boolean userExists(UserData user) throws SQLException {
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "SELECT 1 FROM contacts WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            return stmt.executeQuery().next();
        }
    }

    /**
     * Adds new user to contacts
     * @param user user to be add
     */
    public void addContact(ExUserData user) throws SQLException {
        if(userExists(user))
            return;
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "INSERT INTO contacts (login, userKey, userName) VALUES (?, ?, ?)"
        )) {
            stmt.setString(1,user.login);
            stmt.setBytes(2,user.publicKey);
            stmt.setString(3,user.name);
            stmt.executeUpdate();
        }
    }

    /**
     * Completes given user with missing fields
     * @param user user to complete
     */
    private void complete(ExUserData user) throws SQLException {
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "SELECT * FROM contacts WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            ResultSet rs=stmt.executeQuery();
            if(rs.next()) {
                if(user.name==null) user.name=rs.getString(4);
                if(user.publicKey==null) user.publicKey=rs.getBytes(3);
            }
        }
    }

    /**
     * Updates given user
     * @param user user with updated data
     */
    public void updateUser(ExUserData user) throws SQLException {
        complete(user);
        try(PreparedStatement stmt= dbConnection.prepareStatement(
                "UPDATE contacts SET userKey=?, userName=? WHERE login=?"
        )) {
            stmt.setBytes(1,user.publicKey);
            stmt.setString(2,user.name);
            stmt.setString(3,user.login);
            stmt.executeUpdate();
        }
    }

    /**
     * Deletes user from contacts
     * @param user user to be deleted
     */
    public void removeContact(UserData user) throws SQLException {
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "DELETE FROM contacts WHERE login=?"
        )) {
            stmt.setString(1,user.login);
            stmt.executeUpdate();
        }
    }
}
