package client.network;

import client.security.ConnectionCrypter;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Handles sending messages to server
 * @author Florek
 */
public class Sender extends Thread {
    private DataOutputStream out;       //sending stream
    private BlockingQueue<byte[]> toSend=new LinkedBlockingQueue<>(5);  //queue of messages to be send
    private ConnectionCrypter crypter;  //crypter used for connection

    /**
     * Initializes sender
     * @param connection socket used for connection
     * @param crypter crypter used for connection
     */
    public Sender(Socket connection, ConnectionCrypter crypter) throws IOException {
        out=new DataOutputStream(connection.getOutputStream());
        this.crypter=crypter;
    }

    /**
     * Adds message to send
     * @param data data to be send
     */
    public void addMsg(byte[] data) throws InterruptedException {
        toSend.put(data);
    }

    /**
     * Runs in separate thread
     */
    public void run() {
        try {
            while(!Thread.interrupted()) {
                byte[] data;
                data=crypter.encrypt(toSend.take());
                out.writeInt(data.length);
                out.write(data);
            }
        } catch(InterruptedException | IOException e) {
            //e.printStackTrace();  //both exception could be thrown when disconected so ignore it
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            try { out.close(); }
            catch(IOException ignore) { ignore.printStackTrace(); }
        }
    }
}
