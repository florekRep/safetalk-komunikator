package client.gui;

import client.userDatabase.ArchiveManager;
import protocol.Message;
import protocol.UserData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Controller and GUI for Archive managing
 * @author Florek
 */

public class ArchiveViewer extends JFrame {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    private JList<UserData> list;
    private DefaultListModel<UserData> conversations;
    private JTextArea history;

    private ArchiveManager archive; //reference to db holding archive

    /**
     * Create the frame.
     */
    public ArchiveViewer(ArchiveManager archive) {
        this.archive=archive;

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 600, 500);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        JSplitPane splitPane = new JSplitPane();
        splitPane.setResizeWeight(0.3);
        contentPane.add(splitPane, BorderLayout.CENTER);

        JScrollPane scrollPane_1 = new JScrollPane();
        splitPane.setRightComponent(scrollPane_1);

        Font font=new Font("Helvetica",Font.PLAIN,14);

        history = new JTextArea();
        history.setLineWrap(true);
        history.setFont(font);
        scrollPane_1.setViewportView(history);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setMinimumSize(new Dimension(0, 0));
        splitPane.setLeftComponent(scrollPane);

        conversations = new DefaultListModel<>();

        list=new JList<>();
        list.setModel(conversations);
        list.setFont(font);
        list.setFixedCellHeight(25);

        JPanel additionalPanel=new JPanel();
        additionalPanel.setLayout(new BorderLayout(0, 0));
        additionalPanel.add(list);
        scrollPane.setViewportView(additionalPanel);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.SOUTH);
        panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

        JButton btnDelete = new JButton("Usuń");
        panel.add(btnDelete);

        JButton btnShow = new JButton("Pokaż");
        panel.add(btnShow);

        btnDelete.addActionListener(new DeleteEvent());
        btnShow.addActionListener(new ShowEvent());
    }

    /**
     * Refreshe archive viewer by displaying all users who have messages in database
     */
    public void refresh() {
        try {
            Set<UserData> users=archive.getAllUsers();
            conversations.clear();
            for(UserData user: users)
                conversations.addElement(user);
            list.revalidate();
            list.repaint();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show ArchiveViewer window
     */
    public void showWindow() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                setVisible(true);
                refresh();
            }
        });
    }

    /*   EVENT HANDLERS   */
    private class ShowEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(list.isSelectionEmpty())
                return;
            try {
                history.setText("");
                List<Message> msgs=archive.getMessagesFrom(list.getSelectedValue());
                for(Message msg: msgs)
                    history.append(sdf.format(new Date(msg.time))+" "+msg.from+": "+new String(msg.msg)+'\n');

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private class DeleteEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(list.isSelectionEmpty())
                return;
            try {
                history.setText("");
                archive.deleteHistory(list.getSelectedValue());
                refresh();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
