package protocol;

/**
 * Structure used to hold messages
 */

import java.io.Serializable;

public class Message implements Serializable {
	public String to;
	public String from;
	public byte[] msg;
    public long time;
	public Message(String to, String from, byte[] data, long time) {
		this.to=to;
		this.from=from;
		msg=data;
        this.time=time;
	}
}
