package client.messages;

import client.security.SecurityOptions;
import protocol.SafeMessage;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;

/**
 * Crypter for conversations
 * @author Florek
 */
public class SafeMessageModule {
    //necessary ciphers ang keygens
    private static Cipher keyCrypter = SecurityOptions.getUserKeyCipher();
    private static Cipher convCrypter = SecurityOptions.getMessageCipher();
    private static KeyGenerator convKeygen =SecurityOptions.getMessageKeygen();
    private static Signature signature=SecurityOptions.getSignature();

    /**
     * Check if key was loaded successfully. If not it is unable to decrypt or sgin messages
     * @return is key loaded
     */
    public static boolean isOperational() {
        return userKeys!=null;
    }

    /**
     * Generates new key (ex. when creating new account)
     * @return new par of keys
     */
    public static KeyPair generateNewKeys() {
        return SecurityOptions.getUserKeyGenerator().generateKeyPair();
    }

    /**
     * Invoked when account was successfully created (accepted by server) to save key
     * @param user user name of new account
     * @param key key pair for that user
     */
    public static void accountCreated(String user, KeyPair key){
        try(ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(user+".key"))) {
            oos.writeObject(key);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes module for given user. Tries to load user KeyPair
     * @param user login of user to load key
     * @throws IOException when unable to load key
     */
    public static void init(String user) throws IOException {
        userKeys=null;
        File file=new File(user+".key");
        try(ObjectInputStream ois= new ObjectInputStream(new FileInputStream(file))) {
            userKeys=(KeyPair)ois.readObject();
        } catch (ClassNotFoundException e) { //shouldn't ever happen
            userKeys=null;        //not succesful
            throw new RuntimeException(e);
        }
    }

    //crypting messages

    private static KeyPair userKeys=null; //one logged person, so static field
    //keeps separate key for all conversations, also keep distanceUserKey for easier use...
    private SecretKey conversationKey;
    private PublicKey distantUserKey;

    /**
     * Creates instance for encrypting conversation with user
     * @param distantUserKey key of user with whom logged user speaks
     */
    public SafeMessageModule(PublicKey distantUserKey) { //when opening new safe channel
        conversationKey=convKeygen.generateKey();       //generate key
        this.distantUserKey=distantUserKey;             //init distanceUserKey
    }

    /** enum that describes how decrypt went */
    public enum Validation {CONFIRMED, NOT_CONFIRMED, UNABLE_TO_CONFIRM, KEY_OUT_OF_DATE}

    /**
     * Encrypts given plain message. Encryption includes:
     *   - cyphering text with symmetric alg. (DES)
     *   - cyphering key used to encrypt text with distanceUser public key (RSA)
     *   - signing the message with user private key (if one is available) (RSA with SHA1)
     * @param safeMsg msg to be encrypted
     */
    public synchronized void encode(SafeMessage safeMsg) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, SignatureException {
        keyCrypter.init(Cipher.ENCRYPT_MODE, distantUserKey);           //initialize algorithm to crypt key into Encrypt_mode
        safeMsg.key= keyCrypter.doFinal(conversationKey.getEncoded());  //crypt and write encrypted key to msg
        if(isOperational()) {
            signature.initSign(userKeys.getPrivate());                   //init Signature with user priv key if possible
            signature.update(safeMsg.msg);
            safeMsg.signature=signature.sign();                          //signing message
        } else
            safeMsg.signature=null;
        convCrypter.init(Cipher.ENCRYPT_MODE, conversationKey);         //init algorithm to crypt message itself
        safeMsg.msg= convCrypter.doFinal(safeMsg.msg);                  //crypt and write message to structure
    }

    /**
     * Decrypts given message. Decryption includes:
     *    - decrypting key, with user priv key (RSA)
     *    - decrypt message with symmetric key (DES)
     *    - check signature (if one is available) (RSA with SHA1, distanceUser public key)
     * @param safeMsg msg to decrypt
     */
    public synchronized Validation decode(SafeMessage safeMsg) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, SignatureException {
        if(!isOperational()) {
            safeMsg.msg="Unable to decrypt, no key provided".getBytes();
            return Validation.UNABLE_TO_CONFIRM;
        }
        keyCrypter.init(Cipher.DECRYPT_MODE, userKeys.getPrivate());    //initialize algorithm to crypt key into decrypt_mode
        safeMsg.key= keyCrypter.doFinal(safeMsg.key);                   //acquire key for decryption
        SecretKeySpec receivedKey=new SecretKeySpec(safeMsg.key,"DES"); //reassemble key

        convCrypter.init(Cipher.DECRYPT_MODE, receivedKey);             //init algorithm to decrypt message itself
        safeMsg.msg=convCrypter.doFinal(safeMsg.msg);                   //decrypt message

        if(safeMsg.signature==null)                                    //if signature not included return appropriate info
            return Validation.UNABLE_TO_CONFIRM;

        signature.initVerify(distantUserKey);                          //init signature check with distanceUserKey (pub)
        signature.update(safeMsg.msg);                                 //deliver data signed (decrypted msg)
        boolean verify=signature.verify(safeMsg.signature);            //check validity

        return verify ? Validation.CONFIRMED : Validation.NOT_CONFIRMED; //return appropriate info
    }

    /**
     * Checks whether distance user key was loaded (if not some functionality is lost)
     * @return true if key was found and loaded, false otherwise
     */
    boolean isEncodeAvailable() {
        return distantUserKey!=null;
    }
}
