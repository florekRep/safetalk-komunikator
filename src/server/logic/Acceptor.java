package server.logic;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

/**
 * Handler for accepting new connections
 * @author Florek
 */
public class Acceptor {
	/** selector */
	private Selector selector;
	/** serverSocket */
	private ServerSocketChannel serverSocketChannel;

    /**
     * Constructor sets up server socket channel, and selector
     * @param selector selector
     * @param serverSocketChannel server socket channel
     */
	public Acceptor(Selector selector, ServerSocketChannel serverSocketChannel) {
		this.selector=selector;
		this.serverSocketChannel=serverSocketChannel;
	}

    /**
     * Invoked when new connection is waiting for acceptance.
     */
	public void accept() throws IOException{
		Logger logger=Logger.getLogger("Server");
		//accept new connection
		SocketChannel socketChannel = serverSocketChannel.accept();
		logger.info("Accepted from: "+socketChannel.socket().getRemoteSocketAddress());
		//setting up handler for new connection
		socketChannel.configureBlocking(false);
		socketChannel.register(selector, SelectionKey.OP_READ,
				new ConnectionHandler(selector,socketChannel));
		logger.info("Client ready: "+socketChannel.socket().getRemoteSocketAddress());
	}
}
