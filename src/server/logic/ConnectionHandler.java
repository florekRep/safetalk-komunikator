package server.logic;

import server.requests.ConnectionCrypter;
import server.requests.Dispatcher;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * Connection Hadler. Resposible for handling each connection. Especially
 * reading/writting responses (not raw bytes but WHOLE responses(bytes))
 * @author Florek
 *
 */

public class ConnectionHandler {
	private Logger logger=Logger.getLogger("Server");
	private Charset encoder=Charset.forName("UTF-8");
	//slector for channel
	private Selector selector;
	//actual channel
	private SocketChannel socketChannel;
	
	private Dispatcher dispatcher=new Dispatcher(this);

    /** responses to be send, used concurrent Queue (no need for synch) */
	private Queue<byte[]> msgs=new ConcurrentLinkedQueue<>();
    private ByteBuffer toWrite=ByteBuffer.allocate(512);
	//data read from channel.
	private ByteBuffer readData=ByteBuffer.allocate(128); //ByteBuffer per channel to read
	private int msgSize=0;
	
	public ConnectionHandler(Selector selector, SocketChannel socketChannel) throws IOException{
        this.selector=selector;
		this.socketChannel=socketChannel;
		readData.clear();
        toWrite.flip();
		logger.info("Connection Handler created for: "+socketChannel.getRemoteAddress());
	}
	/**
	 * Dispatches events
	 * @throws IOException
	 */
	public void dispatch(byte[] data) throws IOException {
		dispatcher.addRequest(data);
	}
	
	public ByteBuffer getChannelBuffer() {
		return readData;
	}
	/**
	 * Read and assemble responses send through Network
	 * @param size
	 * @throws IOException
	 */
	public void readMessage(int size) throws IOException {
		logger.info("Reading data from channel: "+socketChannel.getRemoteAddress()+" "+size+" B");
		
		//while there is data to process in
		while(readData.hasRemaining()) {
			if(msgSize==0) {
				if(readData.remaining()<4) {
					readData.compact();
					return;		//if data read doesn't contain int
				}
				msgSize=readData.getInt(); //get Int
				if(msgSize>readData.remaining()) { //check if buffer capacity can hold message
					ByteBuffer bigger=ByteBuffer.allocate(msgSize);
					bigger.put(readData);
					readData=bigger;                //if not get bigger buffer and write old one to new
                    return;                         //wait for rest of the message
				}
			}
			if(readData.remaining()>=msgSize) {   //is there a msg in buffer?
				byte[] msg=new byte[msgSize];     //if so then get one
				readData.get(msg);
				dispatch(msg);                   //and dispatch it
				msgSize=0;		//msg has been read, wait for next
			} else {               //if not, get buffer ready to read to it
				readData.compact();
				return;			//if not enought data in buffer to reassemble the message
			}	
		}
		readData.clear();
	}

	/**
	 * Send responses through channel
	 * @param data
	 */
	public void send(byte[] data) throws IOException {
        if(data.length+4>toWrite.capacity()) {
            ByteBuffer bigger=ByteBuffer.allocate(data.length+4);
            synchronized (toWrite) {
                bigger.put(toWrite);
                toWrite=bigger;
                toWrite.flip();
            }
        }
        if(msgs.isEmpty())
            if(attemptWrite(data))
                return;     //all send
        //not send, add to queue
        msgs.add(data);    //add new data to write

        SelectionKey key=socketChannel.keyFor(selector);
        selector.wakeup();      //wake selector up
        key.interestOps(SelectionKey.OP_WRITE);  //inform selector about pending data.
	}

    private boolean attemptWrite (byte[] data) throws IOException {
        synchronized (toWrite) {       //only one thread can write at the moment
            if(toWrite.hasRemaining()) return false; //just in case some thread stacks here while other will execute this method
            toWrite.clear();
            toWrite.putInt(data.length);
            toWrite.put(data);
            toWrite.flip();
            while(toWrite.hasRemaining())
                if(socketChannel.write(toWrite)==0)
                    return false;
            return true;
        }
    }
	/**
	 * get Queue of responses to be send
	 * @return
	 */
	public Queue<byte[]> getMessages() {
		return msgs;
	}

    public ByteBuffer getToWrite() {
        return toWrite;
    }

    public void connectionClosed() {
        dispatcher.disconnected();
    }

    public ConnectionCrypter getCrypter() {
        return dispatcher.getCrypter();
    }
}
