package client.network;

/**
 * Implemented by class that wants to receive data from socket (some kind of eventProcessor)
 * @author Florek
 */
public interface Dispatcher {
    public void received(byte[] data);  //blocking call
    public void disconnected();     //information about disconnection
}
