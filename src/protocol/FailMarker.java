package protocol;

import java.io.Serializable;

/**
 * Markers that describe failure on server side
 */
public enum FailMarker implements Serializable {
    NotLoggedIn, NotValidInput, SenderNotValid, UserExists, UserNotExists, VerificationFailed, AlreadyLoggedIn, Unknown
}
