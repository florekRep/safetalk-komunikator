package client.messages;

import client.userDatabase.ContactManager;
import protocol.SafeMessage;
import protocol.UserData;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Manager for conversation crypters
 * @author Florek
 */
public class ConversationCrypter {
    private Map<String, SafeMessageModule> crypterModules=new HashMap<>();
    private ContactManager contactManager; //to get keys from

    /**
     * Creates new manager, unique for every logged user
     * @param contactManager contactManager for logged user
     */
    public ConversationCrypter(ContactManager contactManager) {
        this.contactManager=contactManager;
    }

    /** creates new crypter module and attempts to load distance user key from contacts */
    private SafeMessageModule addSafeModule(String user) {
        try {
            byte[] rawKey=contactManager.getUserKey(new UserData(user,null,null));
            PublicKey key=null;
            if(rawKey!=null) //if user is present, reassemble his key
                key= KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(rawKey));
            SafeMessageModule newModule=new SafeMessageModule(key); //starts new safe module
            crypterModules.put(user,newModule); //add it to the map
            return newModule;
        } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Attempts to decrypt given message (may fail for ex. if key was updated and message was encoded using old key)
     * @param msg message to be decrypted
     * @return status of verification
     */
    public SafeMessageModule.Validation decode(SafeMessage msg) {
        try {
            return getCrypter(msg.from).decode(msg);
        } catch (IllegalBlockSizeException  | BadPaddingException | SignatureException | InvalidKeyException e) {
            e.printStackTrace();    //if failed to decrypt message inform user
            return SafeMessageModule.Validation.KEY_OUT_OF_DATE;
        }
    }

    /**
     * Attempts to encrypt given message (may fail for ex. if no key for distance user was found)
     * @param msg message to be encrypted
     * @return true if encryption was possible, false if not
     */
    public boolean encode(SafeMessage msg) {
        try {
            SafeMessageModule module=getCrypter(msg.to);
            if(module.isEncodeAvailable())  //if possible to encode
                module.encode(msg);
            return module.isEncodeAvailable();
        } catch (IllegalBlockSizeException  |BadPaddingException | SignatureException | InvalidKeyException e) {
            e.printStackTrace();
            return false;
        }
    }

    /** returns crypter for given distance user, or create new one if no was found */
    private SafeMessageModule getCrypter(String user) {
        SafeMessageModule module=crypterModules.get(user);
        if(module==null)
            return addSafeModule(user);
        return module;
    }

    /** closes all opened crypters */
    void closeAll() {
        crypterModules.clear();
    }
    /** close given crypter */
    void close(String name) {
        crypterModules.remove(name);
    }

}