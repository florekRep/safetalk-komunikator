package client.security;

import java.security.MessageDigest;

/**
 * Handles password encrypting
 */
public class PasswordProcessor {
    private static MessageDigest SHA1=SecurityOptions.getHash();

    /**
     * returns hash for given password
     * @param pass password
     * @return hash for password
     */
    public static byte[] hashPassword(char[] pass) {
        SHA1.reset();
        SHA1.update(new String(pass).getBytes());
        return SHA1.digest();
    }
}
