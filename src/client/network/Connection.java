package client.network;

import client.protocol.ProtocolProcessor;
import client.responses.KeyListener;
import client.security.ConnectionCrypter;
import protocol.Protocol;
import protocol.Request;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Encapsulates connection to the server
 * @author Florek
 */
public class Connection {
    private Socket socket;
    private Sender sender;
    private Receiver receiver;
    private ConnectionCrypter crypter;

    /**
     * Creates connection with given host
     * @param address host
     * @param port port
     */
    public Connection(String address,int port) throws IOException{
        socket=new Socket(InetAddress.getByName(address),port);
        crypter=new ConnectionCrypter();
        sender=new Sender(socket,crypter);
        receiver=new Receiver(socket,crypter);
    }

    /**
     * Sends protocol message to server
     * @param message message to send
     */
    public void send(Protocol message) {
        try {
            byte[] data= ProtocolProcessor.prepareToSend(message);
            sender.addMsg(data);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets dispatcher for receiving data from server
     * @param dispatcher dispatcher for messages
     */
    public void setDispatcher(Dispatcher dispatcher) {
        receiver.setDispatcher(dispatcher);
    }

    /**
     * Returns KeyListener for receiving key
     * @return listener for key
     */
    public KeyListener getKeyListener() {
        return crypter;
    }

    /**
     * Starts receiving end sending. Also initializes handshake between server and client
     */
    public void start() throws IOException {
        sender.start();
        receiver.start();
        send(new Protocol(Request.KEY,crypter.getHandshakeKey()));  //starts handshake
    }

    /**
     * Stops receiving and sending
     */
    public void close() {
        try {
            receiver.interrupt();
            sender.interrupt();
            socket.close();
        } catch(IOException ignore) {
            ignore.printStackTrace();
        }
    }
}
