package client.responses;

import protocol.FailMarker;

/**
 * Interface that is implemented by classes that want to react to server responses
 * @author Florek
 */
public interface ResponseListener {
    public void requestSuccessful(Object received);
    public void requestFailed(FailMarker marker);
}
