package client.responses;

import protocol.Message;

/**
 * Interface that is implemented by classes that want to react to receiving message (and states associated with it)
 * @author Florek
 */
public interface MessageListener {
    public void messageReceived(Message msg);
    public void messageSent(String to);
    public void messageFailed(String to);
}
