package server.database;

import server.messages.MessageManager;
import server.users.PasswordProcessor;
import server.users.UserDB;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Initializes connection to database
 * @author Florek
 *
 */
public class DBConnection {
    private static Connection connection;
	private static final String DRIVER="org.sqlite.JDBC";
	private static final String DBNAME="jdbc:sqlite:users.db";
	private static Logger logger=Logger.getLogger("Server");

    /**
     * Init connection, and then all managers
     * @throws SQLException
     */
	public static void init() throws SQLException {
		try {
			Class.forName(DRIVER);
			logger.info("Driver loaded");
            connection=DriverManager.getConnection(DBNAME);
            //init managers
            UserDB.init(connection);
            MessageManager.init(connection);
            PasswordProcessor.init();
		} catch(ClassNotFoundException | NoSuchAlgorithmException e) { //should NEVER happen
			logger.severe(e.getMessage());
			close();
        }
    }

    /**
     * Close database connection
     */
	public static void close() {
        try {
            logger.info("Closing dataBaseConnection");
            connection.close();
            MessageManager.close();
            UserDB.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
