package protocol;

/**
 * Simple structure to hold user data
 */

import java.io.Serializable;

public class UserData implements Serializable {
	public String login; 
	public byte[] pass;
	public String name;

    public UserData(String login, String name) {
        this(login,null,name);
    }

	public UserData(String login, byte[] pass, String name) {
		this.login=login;
		this.pass=pass;
		this.name=name;
	}

    @Override
    public boolean equals(Object obj) {
        return obj instanceof UserData && login.equals(((UserData)obj).login);
    }

    @Override
    public String toString() {
        return name!=null ? name+" ("+login+")": login;
    }

    @Override
    public int hashCode() {
        return login.hashCode();
    }
}
