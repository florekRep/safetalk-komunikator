package client.gui;

/**
 * Used to display ongoing conversation
 * @author Florek
 */

import client.messages.MessagesDispatcher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;

public class MessageWindow extends JFrame {
    private JTextArea writing;
    private JTextArea conversation;
    private JLabel lblStatus;
    private JButton btnSend;
    private JCheckBox chckbxEnter;
    private JCheckBox safeChannel;
    private JLabel lblInfo;

    private MessagesDispatcher dispatcher;  //reference to controller of that window
    private String to;   //name of distance user

	/**
	 * Create the frame.
	 */
	public MessageWindow(MessagesDispatcher dispatcher, String to) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 500);
        JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setDividerSize(10);
		splitPane.setResizeWeight(0.7);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		contentPane.add(splitPane, BorderLayout.CENTER);

		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);

        writing = new JTextArea();
		scrollPane.setViewportView(writing);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		splitPane.setLeftComponent(scrollPane_1);
		
		conversation = new JTextArea();
        conversation.setEditable(false);
		scrollPane_1.setViewportView(conversation);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		btnSend = new JButton("Wyślij");
		panel.add(btnSend);
		
		JButton btnClear = new JButton("Wyczyść");
		panel.add(btnClear);

        chckbxEnter = new JCheckBox("Enter");
		panel.add(chckbxEnter);

        safeChannel=new JCheckBox("safeMode");
        panel.add(safeChannel);

        lblStatus = new JLabel();
		panel.add(lblStatus);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		lblInfo = new JLabel("Info:");
		panel_1.add(lblInfo);

        Font font=new Font("Helvetica",Font.PLAIN,12);
        writing.setFont(font);
        conversation.setFont(font);
        this.dispatcher=dispatcher;
        this.to=to;
        setTitle(to);
        setStatus("Idle");

        btnSend.addActionListener(new WritingEvent());
        btnClear.addActionListener(new ClearEvent());
        writing.addKeyListener(new EnterEvent());
        addWindowListener(new CloseEvent());
	}

    /**
     * Write given message on conversation text field
     * @param msg message to be written
     */
    public void writeMessage(String msg) {
        conversation.append(msg+'\n');
    }

    /**
     * Set status to given message
     * @param status message
     */
    public void setStatus(String status) {
        lblStatus.setText("Status: "+status);
    }

    /**
     * Set information to given message
     * @param info mesage
     */
    public void setInfo(String info) {
        lblInfo.setText("Info: "+info);
    }

    /*    EVENT HANDLERS   */
    private class EnterEvent extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            if(chckbxEnter.isSelected() && e.getKeyCode()==KeyEvent.VK_ENTER) {
                e.consume();    //consume that event
                btnSend.doClick();
            }
            else
                super.keyTyped(e);
        }
    }

    private class ClearEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            conversation.setText("");
            setStatus("Cleared");
        }
    }

    private class WritingEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Date time=new Date();
            dispatcher.send(MessageWindow.this, to, writing.getText(),time,safeChannel.isSelected());
            writing.setText("");
        }
    }

    private class CloseEvent extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent e) {
            dispatcher.closing(to);
        }
    }
}
