package protocol;

import java.io.Serializable;

/**
 * Extended structure to hold message (with security measures)
 */
public class SafeMessage extends Message implements Serializable {
    public byte[] key;
    public byte[] signature;
    public SafeMessage(String to, String from, byte[] msg, long time, byte[] key, byte[] signature ) {
        super(to,from,msg,time);
        this.key=key;
        this.signature=signature;
    }

}
