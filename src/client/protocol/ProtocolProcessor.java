package client.protocol;

import protocol.Protocol;

import java.io.*;
import java.util.logging.Logger;

/**
 * Converts Raw bytes to Protocol Command and vice versa
 * @author Florek
 */
public class ProtocolProcessor {
    /**
     * Translates protocol object to byte array (serialization)
     * @param protocol protocol to serialize
     * @return bytes to be send, that represent Protocol object
     */
	public static byte[] prepareToSend(Protocol protocol) throws IOException{
        ByteArrayOutputStream out=new ByteArrayOutputStream();	//need to use ByteArrayStream (only way with standard serialization)
        try(ObjectOutputStream oos=new ObjectOutputStream(out)){
            oos.writeObject(protocol);	//return byte[] containing object
            return out.toByteArray();
        } finally {
            try {
                out.close();				//no need
            } catch(Exception ignore) {ignore.printStackTrace();}
        }
	}

    /**
     * Translate byte array to Protocol object
     * @param data data to be translated into Protocol
     * @return deserialized Protocol
     */
	public static Protocol getProtocol(byte[] data) throws IOException {
		ByteArrayInputStream in=new ByteArrayInputStream(data);
		try(ObjectInputStream ooi=new ObjectInputStream(in)) {
            return (Protocol)ooi.readObject();
		} catch (ClassNotFoundException e) {
			Logger logger=Logger.getLogger("Server");
			logger.severe(e.getMessage());
			throw new RuntimeException(e);
		} finally {
            try {
                in.close();					//no need
            } catch(Exception ignore) {ignore.printStackTrace();}
		}
	}
}
//Uses standard serialization. Given byte arrays are pretty big. Should be changed