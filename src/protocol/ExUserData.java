package protocol;

import java.io.Serializable;

/**
 * Extended structure that describes user
 */
public class ExUserData extends UserData implements Serializable {
    public byte[] newPass;
    public byte[] publicKey;


    public ExUserData(String login, byte[] pass, String name, byte[] publicKey) {
        this(login, pass, name, publicKey, null);
    }
    public ExUserData(String login, byte[] pass, String name, byte[] publicKey, byte[] newPass) {
        super(login,pass,name);
        this.publicKey=publicKey;
        this.newPass=newPass;
    }
}
