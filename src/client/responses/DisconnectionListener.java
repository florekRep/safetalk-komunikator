package client.responses;

/**
 * Interface that is implemented by classes that want to react to server disconnection
 * @author Florek
 */
public interface DisconnectionListener {
    public void disconnected();
}
