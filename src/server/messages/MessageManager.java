package server.messages;

import protocol.Message;
import protocol.SafeMessage;

import java.sql.*;
import java.util.LinkedList;
import java.util.logging.Logger;

/**
 * Manages message that wait for user to be logged in
 * @author Florek
 */
public class MessageManager {
    private static MessageManager dataBase;
    private static Logger logger=Logger.getLogger("Server");

    private Connection conn; //connection to database
    /** Constructor initializes connection */
    private MessageManager(Connection conn) {
        this.conn=conn;
    }

    /**
     * Return instance of manager (Singleton)
     * @return instance of MessageManager
     */
    public static MessageManager getMessageManager() {
        if(dataBase==null) {
            logger.severe("MessageManager not initialized");
            throw new RuntimeException("MessageManager not initialized");
        }
        return dataBase;
    }

    /**
     * Initializes manager (create table)
     * @param conn connection to database
     */
    public static void init(Connection conn) throws SQLException {
        dataBase=new MessageManager(conn);
        try (PreparedStatement stmt=conn.prepareStatement(
            "CREATE TABLE IF NOT EXISTS messages (" +
            "id INTEGER NOT NULL PRIMARY KEY, " +
            "to_user VARCHAR(64) NOT NULL, " +
            "from_user VARCHAR(64) NOT NULL, " +
            "send_time INTEGER NOT NULL, " +
            "message BLOB NOT NULL, " +
            "user_key BLOB, signature BLOB)"
        )) {
            stmt.executeUpdate();
        } catch(SQLException e) {
            close();
            throw e;
        }
    }

    /**
     * Store message in database
     * @param msg message to be stored
     */
    public void storeMessage(Message msg) throws SQLException{
       try(PreparedStatement stmt=conn.prepareStatement(
          "INSERT INTO messages (to_user, from_user, send_time, message, user_key, signature) VALUES(?,?,?,?,?,?)"
       )) {
            stmt.setString(1, msg.to);
            stmt.setString(2,msg.from);
            stmt.setLong(3,msg.time);
            stmt.setBytes(4,msg.msg);
            if(msg instanceof SafeMessage) {    //Safe message need special storage
                SafeMessage sm=(SafeMessage)msg;
                stmt.setBytes(5, sm.key);
                stmt.setBytes(6, sm.signature);
            } else {
                stmt.setNull(5, Types.BLOB);
                stmt.setNull(6, Types.BLOB);
            }
            stmt.executeUpdate();
       }
    }

    /**
     * Return list of waiting messages for given user
     * @param login user
     * @return list of messages
     */
    public LinkedList<Message> getWaitingMessages(String login) throws SQLException {
        LinkedList<Message> msgs=new LinkedList<>();
        try(PreparedStatement stmt=conn.prepareStatement(
            "SELECT * FROM messages WHERE to_user=? ORDER BY send_time"
        )) {
            stmt.setString(1, login);
            ResultSet rs=stmt.executeQuery();
            while(rs.next()) {
                byte[] key=rs.getBytes(6);  //first check if key is null...
                Message msg;
                if(rs.wasNull()) //... if so it is normal message ...
                    msg=new Message(rs.getString(2), rs.getString(3), rs.getBytes(5), rs.getLong(4));
                else  //... otherwise safe message
                    msg=new SafeMessage(rs.getString(2), rs.getString(3), rs.getBytes(5), rs.getLong(4), key, rs.getBytes(7));
                msgs.add(msg);
            }
        }
        try(PreparedStatement stmt=conn.prepareStatement(
            "DELETE FROM messages WHERE to_user=?"
        )) {
            stmt.setString(1,login);        //delete retrieved messages
            stmt.executeUpdate();
        }
        return msgs;
    }

    /**
     * Check if user has any waiting messages for him
     * @param login user to check messages for
     * @return true if he has waiting messages, false otherwise
     */
    public boolean hasWaitingMessages(String login) throws SQLException {
        try(PreparedStatement stmt=conn.prepareStatement(
                "SELECT 1 FROM messages WHERE to_user=?"
        )) {
            stmt.setString(1,login);
            return stmt.executeQuery().next();
        }
    }

    /**
     * Close manager
     */
    public static void close() {
        logger.info("Closing database");
        dataBase=null;
    }
}
