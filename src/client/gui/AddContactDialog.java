package client.gui;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class AddContactDialog extends JDialog {
    private final JPanel contentPanel = new JPanel();
    private JTextField loginField;
    private JTextField nameField;

    /**
     * Show dialog and waits for input
     * @param parent parent window
     * @return user action
     */
    public int showAddContactDialog(Component parent) {
        loginField.setText("");
        nameField.setText("");
        return JOptionPane.showConfirmDialog(parent,contentPanel,"Dodaj kontakt",JOptionPane.OK_CANCEL_OPTION);
    }

    /**
     * Return entered login
     * @return login
     */
    public String login() {
        return loginField.getText().equals("") ? null : loginField.getText();
    }

    /**
     * Return entered name
     * @return name
     */
    public String name() {
        return nameField.getText().equals("") ? null : nameField.getText();
    }

    /**
     * Create the dialog.
     */
    public AddContactDialog() {
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new MigLayout("", "[][grow]", "[][]"));
        {
            JLabel lblLogin = new JLabel("Login");
            contentPanel.add(lblLogin, "cell 0 0,alignx trailing");
        }
        { //
            loginField = new JTextField();
            contentPanel.add(loginField, "cell 1 0,growx");
            loginField.setColumns(20);
        }
        {
            JLabel lblName = new JLabel("Nazwa");
            contentPanel.add(lblName, "cell 0 1,alignx trailing");
        }
        {
            nameField = new JTextField();
            contentPanel.add(nameField, "cell 1 1,growx");
            nameField.setColumns(20);
        }
        pack();
    }

}
