package server.users;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Performs operation on password (hash, salting)
 */
public class PasswordProcessor {
    private static PasswordProcessor instance;

    /**
     * Return instance to processor (Singleton)
     * @return instance of PasswordProcessor
     */
    public static PasswordProcessor getInstance() {
        return instance;
    }

    /**
     * Init password processor
     */
    public static void init() throws NoSuchAlgorithmException {
        instance=new PasswordProcessor();
    }

    private SecureRandom random;
    private MessageDigest hash;

    /** Constructor initializes algorithm */
    private PasswordProcessor() throws NoSuchAlgorithmException {
        random =new SecureRandom();
        hash=MessageDigest.getInstance("SHA1");
    }

    /**
     * Return random salt
     * @return salt
     */
    public synchronized byte[] getRandomSalt() {
        byte salt[] = new byte[20];
        random.nextBytes(salt);
        return salt;
    }

    /**
     * Salt and hash password
     * @param pass password to be processed
     * @param salt salt for password
     * @return hashed and salted password
     */
    public synchronized byte[] hash(byte[] pass, byte[] salt) {
        hash.reset();
        hash.update(salt);
        return hash.digest(pass);
    }

    /**
     * Compare hashed password, and raw password
     * @param hash hashed password
     * @param salt salt used for hashing
     * @param pass raw password
     * @return true if password is a match, false otherwise
     */
    public synchronized boolean verify(byte[] hash, byte[] salt, byte[] pass) {
        return Arrays.equals(hash,hash(pass,salt));
    }
}