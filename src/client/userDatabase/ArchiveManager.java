package client.userDatabase;

import protocol.Message;
import protocol.UserData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Manages archive for user
 * @author Florek
 */
public class ArchiveManager {
    /** connection to database */
    private Connection dbConnection;

    /**
     * Initializes Archive Manager with given connection (unique for every user)
     * @param conn connection to database
     */
    ArchiveManager(Connection conn) throws SQLException {
        dbConnection=conn;
        try(PreparedStatement stmt=dbConnection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS archive (" +
                "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "toUser VARCHAR(64) NOT NULL, " +
                "fromUser VARCHAR(64) NOT NULL, " +
                "sendTime INTEGER NOT NULL, "+
                "message BLOB NOT NULL"+
                ")"
        )) {
            stmt.executeUpdate();
        }
    }

    /**
     * Adds message to archive
     * @param msg message to add
     */
    public void addMessage(Message msg) {
        try (PreparedStatement stmt= dbConnection.prepareStatement(
            "INSERT INTO archive (toUser,fromUser,sendTime, message) VALUES (?,?,?,?)"
        )) {
            stmt.setString(1,msg.to);
            stmt.setString(2,msg.from);
            stmt.setLong(3,msg.time);
            stmt.setBytes(4,msg.msg);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Retrieves messages send or received from given user
     * @param user user to get messages for
     * @return list of messages for given user
     */
    public LinkedList<Message> getMessagesFrom(UserData user) throws SQLException {
        LinkedList<Message> messages=new LinkedList<>();
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "SELECT * FROM archive WHERE fromUser=? OR toUser=?"
        )) {
            stmt.setString(1,user.login);
            stmt.setString(2,user.login);
            ResultSet rs=stmt.executeQuery();
            while(rs.next())
                messages.add(new Message(rs.getString(2), rs.getString(3),rs.getBytes(5), rs.getLong(4)));
        }
        return messages;
    }

    /**
     * Gets set of users that contacted logged user (have their messages in archive)
     * @return set of users
     */
    public Set<UserData> getAllUsers() throws SQLException {
        Set<UserData> users = new HashSet<>();
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "SELECT DISTINCT toUser FROM archive"
        )) {
            ResultSet rs=stmt.executeQuery();
            while (rs.next())
                users.add(new UserData(rs.getString(1),null,null));
        }
        try(PreparedStatement stmt= dbConnection.prepareStatement(
                "SELECT DISTINCT fromUser FROM archive"
        )) {
            ResultSet rs=stmt.executeQuery();
            while (rs.next())
                users.add(new UserData(rs.getString(1),null,null));
        }
        return users;
    }

    /**
     * Deletes all messages sent or received from given user
     * @param user user that messages will be deleted
     */
    public void deleteHistory(UserData user) throws SQLException {
        try(PreparedStatement stmt= dbConnection.prepareStatement(
            "DELETE FROM archive WHERE toUser=? or fromUser=?"
        )) {
            stmt.setString(1,user.login);
            stmt.setString(2,user.login);
            stmt.executeUpdate();
        }
    }
}
