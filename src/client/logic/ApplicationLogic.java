package client.logic;

import client.gui.ArchiveViewer;
import client.messages.MessagesDispatcher;
import client.messages.SafeMessageModule;
import client.network.Connection;
import client.protocol.FailDecoder;
import client.responses.DisconnectionListener;
import client.responses.ResponseListener;
import client.responses.ServerEventQueue;
import client.userDatabase.DBConnection;
import protocol.*;

import java.io.IOException;
import java.security.KeyPair;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Implements client logic
 * @author Florek
 */
public class ApplicationLogic implements DisconnectionListener {
    //default values for connection
    private static final String HOST="localhost";
    private static final int PORT=3000;

    private Connection connection;
    private MessagesDispatcher messagesDispatcher;
    private ServerEventQueue serverEventQueue;
    private DBConnection dbConnection;
    private ArchiveViewer archiveViewer;
    private UserData logged=null; //logged user

    private final MainFrameNotify frame;

    /**
     * Creates application logic, and sets up information display
     * @param frame frame that will display info
     */
    public ApplicationLogic(MainFrameNotify frame) {
        this.frame=frame;
    }

    /**
     * Invoked when it is necessary to wait for server response to given protocol
     * @param protocol protocol to be send
     */
    public void sendAndWaitForResponse(Protocol protocol) {
        serverEventQueue.registerForResponse();
        connection.send(protocol);
        serverEventQueue.waitForResponse();
    }

    /* ACTIONS */

    /**
     * Connects client to the server. Performs all necessary initializations.
     */
    public void connect() {
        try {
            //lazy initialization
            connection=new Connection(HOST,PORT);       //create connection if non exists
            serverEventQueue=new ServerEventQueue();    //create server event dispatcher
            connection.setDispatcher(serverEventQueue); //set up receiver for data from server (queue)
            serverEventQueue.setKeyListener(connection.getKeyListener()); //set up listener for key from server
            serverEventQueue.start();                   //start server event dispatcher
            connection.start();                         //start connection
            serverEventQueue.setDisconnectedListener(this);
            frame.setStatus("Connected");
        } catch (IOException e) {
            e.printStackTrace();
            frame.failed(e.getMessage());
            frame.setStatus("Connection Failed");
        }
    }

    /**
     * Invoked after client lost connection with server
     */
    @Override
    public void disconnected() {    //reacts for sudden lost of connection
        logged=null;
        disconnect();
        frame.setStatus("Connection lost");
    }

    /** finishes properly disconnection */
    private void finishDisconnection() {
        if(serverEventQueue!=null) {
            serverEventQueue.setDisconnectedListener(null);
            serverEventQueue.interrupt();   //stop server event dispatching
        }
        if(connection!=null) connection.close();             //stop connection
        frame.setStatus("Disconnected");
        frame.refresh();
    }

    /**
     * Disconnects cleanly client from server, with logout
     */
    public void disconnect() {
        if(logged!=null) {
            serverEventQueue.registerForResponse();
            logout();
            serverEventQueue.waitForResponse(); //wait until done
        }
        finishDisconnection();
    }

    /**
     * Login user to server
     * @param user user to be logged
     */
    public void login(final UserData user) {
        if(connection==null) {
            frame.failed("Nie połączony");
            return;
        }
        if(logged!=null)
            return;
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                logged=user;
                loadUserPrivates();
                frame.setStatus("Logged in as: "+logged.login);
                frame.refresh();
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Logging failed");
            }
        });
        connection.send(new Protocol(Request.LOGIN,user));
    }

    /** initializes all user dependent resources (contacts, archive) */
    private void loadUserPrivates() {
        try {
            dbConnection = new DBConnection(logged.login);
            archiveViewer=new ArchiveViewer(dbConnection.getArchiveDB());
            messagesDispatcher=new MessagesDispatcher(connection,logged.login,dbConnection.getArchiveDB(),dbConnection.getContactsDB());
            serverEventQueue.setMessageListener(messagesDispatcher);
            SafeMessageModule.init(logged.login);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            frame.info("Key file nie odnaleziony! Część funkcjonalności utracona");
        }
    }

    /**
     * Logout logged user
     */
    public void logout() {
        if(logged==null)
            return;
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                logged=null;
                unloadUserPrivates();
                frame.setStatus("Logout successful");
                frame.refresh();
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Logout failed");
            }
        });
        connection.send(new Protocol(Request.LOGOUT,logged));
    }

    /** releases all user dependent resources */
    private void unloadUserPrivates() {
        messagesDispatcher.closeAll();
        dbConnection.close();
        dbConnection=null;
        archiveViewer=null;
    }

    /**
     * Create new account
     * @param user data for new account
     */
    public void createAccount(final ExUserData user) {
        if(connection==null) return;
        final KeyPair keys= SafeMessageModule.generateNewKeys();
        user.publicKey=keys.getPublic().getEncoded();
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                SafeMessageModule.accountCreated(user.login, keys);
                frame.setStatus("Account created: "+user.login);
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Creating account failed");
            }
        });
        connection.send(new Protocol(Request.ADD_USER,user));
    }

    /**
     * Updates account
     * @param user new data
     * @param key whether to generate new key pair or not
     */
    public void updateAccount(final ExUserData user,final boolean key) {
        if(logged==null)
            return;
        user.login=logged.login;
        final KeyPair newKeys;
        if(key) {
            newKeys=SafeMessageModule.generateNewKeys();
            user.publicKey=newKeys.getPublic().getEncoded();
        } else
            newKeys=null;
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                logged.pass=user.pass;
                logged.name=user.name;
                if(key) {
                    try {
                        SafeMessageModule.accountCreated(logged.login,newKeys);
                        SafeMessageModule.init(logged.login);   //reload key
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                frame.setStatus("Account updated");
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Updating Failed");
            }
        });
        connection.send(new Protocol(Request.UPDATE_USER, user));
    }

    /**
     * Delete account
     * @param user confirmation data
     */
    public void deleteAccount(final UserData user) {
        if(logged==null)
            return;
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                logged=null;
                unloadUserPrivates();
                frame.setStatus("Account deleted");
                frame.refresh();
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Deleting failed");
            }
        });
        connection.send(new Protocol(Request.REMOVE_USER, user));
    }

    /**
     * Start conversation with selected user
     * @param user selected user
     */
    public void startConversation(UserData user) {
        messagesDispatcher.startConversation(user.login);
    }

    /**
     * Add new user to contacts list
     * @param userData new user to be added
     */
    public void addContact(final ExUserData userData) {
        if(logged==null)
            return;
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                final ExUserData result=(ExUserData)received;
                if(userData.name!=null)
                    result.name=userData.name;
                try {
                    dbConnection.getContactsDB().addContact(result);
                    frame.setStatus("Contact added: "+result.login);
                } catch (SQLException e) {
                    frame.failed(e.getMessage());
                    frame.setStatus("Adding contact failed");
                }
                frame.refresh();
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Adding contact failed");
            }
        });
        connection.send(new Protocol(Request.GET_USER,userData));
    }

    /**
     * Delete user from contacts
     * @param userData user to be deleted
     */
    public void deleteContact(final UserData userData) {
        if(logged==null)
            return;
        try {
            dbConnection.getContactsDB().removeContact(userData);
            frame.setStatus("Contact deleted");
            frame.refresh();
        } catch (SQLException e) {
            frame.failed(e.getMessage());
            frame.setStatus("Deleting failed");
        }
    }

    /**
     * Updates user data from server (mostly used to update user key)
     * @param userData user to be updated
     */
    public void updateContact(final ExUserData userData) {
        if(logged==null)
            return;
        serverEventQueue.setResponseListener(new ResponseListener() {
            @Override
            public void requestSuccessful(Object received) {
                final ExUserData result=(ExUserData)received;
                if(userData.name==null || userData.name.equals(""))
                    result.name=userData.name;
                try {
                    dbConnection.getContactsDB().updateUser(result);
                    frame.setStatus("Contact updated");
                } catch (SQLException e) {
                    frame.failed(e.getMessage());
                    frame.setStatus("Updating failed");
                }
            }
            @Override
            public void requestFailed(FailMarker marker) {
                frame.failed(FailDecoder.decodeFailMarker(marker));
                frame.setStatus("Updating failed");
            }
        });
        messagesDispatcher.closeAll();
        connection.send(new Protocol(Request.GET_USER, userData));
    }

    /**
     * Returns user contact list
     * @return contacts
     */
    public List<UserData> getContacts() {
        if(dbConnection==null)
            return new LinkedList<>();
        try {
            return dbConnection.getContactsDB().getContacts();
        } catch (SQLException e) {
            frame.failed(e.getMessage());
            e.printStackTrace();
        }
        return new LinkedList<>();
    }

    /**
     * Returns user archive viewer
     * @return archive viewer
     */
    public ArchiveViewer getArchiveViewer() {
        return archiveViewer;
    }

}