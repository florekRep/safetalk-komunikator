package client.gui;

import client.security.PasswordProcessor;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Arrays;

public class LoginDialog extends JDialog {
	private final JPanel contentPanel = new JPanel();
	private JTextField loginField;
	private JPasswordField passField;

    /**
     * Show dialog and waits for input
     * @param parent parent window
     * @return user action
     */
    public int showLoginDialog(Component parent) {
        loginField.setText("");
        passField.setText("");
        return JOptionPane.showConfirmDialog(parent,contentPanel,"Login",JOptionPane.OK_CANCEL_OPTION);
    }

    /**
     * Return entered login
     * @return login
     */
    public String login() {
        return loginField.getText().equals("") ? null : loginField.getText() ;
    }

    /**
     * Return entered password
     * @return hashed password
     */
    public byte[] password() {
        if(passField.getPassword().length==0)
            return null;
        byte[] res = PasswordProcessor.hashPassword(passField.getPassword());
        Arrays.fill(passField.getPassword(), '\0');
        return res;
    }

	/**
	 * Create the dialog.
	 */
	public LoginDialog() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][grow]", "[][]"));
		{
			JLabel lblLogin = new JLabel("Login");
			contentPanel.add(lblLogin, "cell 0 0,alignx trailing");
		}
		{
			loginField = new JTextField();
			contentPanel.add(loginField, "cell 1 0,growx");
			loginField.setColumns(20);
		}
		{
			JLabel lblPass = new JLabel("Has\u0142o");
			contentPanel.add(lblPass, "cell 0 1,alignx trailing");
		}
		{
			passField = new JPasswordField();
			contentPanel.add(passField, "cell 1 1,growx");
			passField.setColumns(20);
		}
		pack();
	}

}
