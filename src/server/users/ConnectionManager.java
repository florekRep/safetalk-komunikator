package server.users;

import protocol.UserData;
import server.exceptions.AlreadyLoggedIn;
import server.exceptions.NotLoggedIn;
import server.exceptions.UserNotExists;
import server.exceptions.VerificationFailed;
import server.logic.ConnectionHandler;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

/**
 * Holds connectionHandlers for logged users
 * @author Florek
 */
public class ConnectionManager {
    private static Logger logger= Logger.getLogger("Server");   //for logging purpose, not used very much...
    private ConcurrentMap<String,ConnectionHandler> loggedUsers=new ConcurrentHashMap<>();  //map of logged users (Thread-safe)
    private static ConnectionManager connectionManager=new ConnectionManager();
    private ConnectionManager() {}

    /**
     * Return instance of ConnectionManager (Singleton)
     * @return instance of ConnectionManager
     */
    public static ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    /**
     * Login user
     * @param ud user to be login
     * @param connection connectionHandler associated with user
     */
    public void loginUser(UserData ud,ConnectionHandler connection) throws SQLException, VerificationFailed, AlreadyLoggedIn, UserNotExists {
        if(!UserDB.getUserDB().verifyUser(ud))
            throw new VerificationFailed();
        if(loggedUsers.containsKey(ud.login))
            throw new AlreadyLoggedIn();
        loggedUsers.put(ud.login,connection);
    }

    /**
     * Logout user
     * @param ud user to be logout
     */
    public void logoutUser(UserData ud) throws SQLException, VerificationFailed, NotLoggedIn, UserNotExists {
        if(!isLoggedIn(ud.login))
            throw new NotLoggedIn();
        if(!UserDB.getUserDB().verifyUser(ud))
            throw new VerificationFailed();
        loggedUsers.remove(ud.login);
    }

    /**
     * handles sudden client disconnection without logout
     * @param login user that lost connection
     */
    public void handleDisconnection(String login) {
        loggedUsers.remove(login);
    }

    /**
     * Check if user is logged in
     * @param login user login
     * @return true if is logged in, false otherwise
     */
    public boolean isLoggedIn(String login) {
        return loggedUsers.containsKey(login);
    }

    /**
     * Retrieves connection handler associated with given user
     * @param login user
     * @return user connection handler
     */
    public ConnectionHandler getConnection(String login) {
        return loggedUsers.get(login);
    }

}
